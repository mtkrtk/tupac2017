#import <Foundation/Foundation.h>
#import "OpenCV.h"

@class Camera;

@protocol CameraDelegate
@required

- (void)cameraDidConnect:(Camera *)camera;
- (void)camera:(Camera *)camera didCaptureFrame:(OpenCVFrame *)frame;

@end

@interface Camera : NSObject <OpenCVDelegate>
{
    OpenCV *openCV;
}

- (instancetype)initWithDelegate:(id <CameraDelegate>)delegate;
- (void)start;

@end
