#import "Camera.h"

@implementation Camera
{
    NSOperationQueue *opQueue;
    id <CameraDelegate> delegate;
}

- (instancetype)init
{
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

- (instancetype)initWithDelegate:(id <CameraDelegate>)aDelegate
{
    if (self = [super init]) {
        opQueue = [[NSOperationQueue alloc] init];
        opQueue.name = @"CameraCapturingQueue";
        delegate = aDelegate;
        openCV = [[OpenCV alloc] initWithDelegate:self];
    }
    return self;
}

- (void)start
{
    [opQueue addOperationWithBlock:^{
        [openCV startWithPath:nil];
    }];
}

- (void)openCVDidConnectToSource:(OpenCV *)cv
{
    [delegate cameraDidConnect:self];
}

- (void)openCVDidFailToConnect:(OpenCV *)cv
{
    
}

- (void)openCV:(OpenCV *)cv didCaptureFrame:(OpenCVFrame *)frame
{
    [delegate camera:self didCaptureFrame:frame];
}

@end
