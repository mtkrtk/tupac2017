#import "Camera.h"
#import <GCDAsyncUdpSocket.h>

@interface GoPro : Camera <GCDAsyncUdpSocketDelegate>

- (void)startWithMacAddress:(NSString *)mac;

@end
