#import "GoPro.h"

@implementation GoPro
{
    GCDAsyncUdpSocket *socket;
    dispatch_queue_t dispatchQueue;
    NSData *wolPacket;
}

- (void)startWithMacAddress:(NSString *)mac
{
    uint8_t wol[102];
    memset(wol, 0xFF, 6);
    NSArray *macComponents = [mac componentsSeparatedByString:@":"];
    if (macComponents.count != 6) {
        return;
    }
    uint8_t rawMac[6];
    for (int i = 0; i < 6; ++i) {
        unsigned int tmp;
        [[NSScanner scannerWithString:macComponents[i]] scanHexInt:&tmp];
        rawMac[i] = tmp;
    }
    for (int i = 0; i < 16; ++i) {
        memcpy(&wol[6 * i + 6], rawMac, 6);
    }
    wolPacket = [NSData dataWithBytes:wol length:sizeof(wol)];
    
    dispatchQueue = dispatch_queue_create("GoProOperationQueue", DISPATCH_QUEUE_CONCURRENT);
    socket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatchQueue];
    [socket enableBroadcast:YES error:nil];
    
    dispatch_async(dispatchQueue, ^{
        NSData *data = [@"_GPHD_:0:0:2:0.000000\n" dataUsingEncoding:NSUTF8StringEncoding];
        while (1) {
            [NSThread sleepForTimeInterval:2];
            [socket sendData:data toHost:@"10.5.5.9" port:8554 withTimeout:-1 tag:0];
        }
    });
    [self connect];
}

- (void)connect
{
    dispatch_async(dispatchQueue, ^{
        NSURLSession *session = [NSURLSession sharedSession];
        NSURL *restartURL = [NSURL URLWithString:
                             @"http://10.5.5.9/gp/gpControl/execute?p1=gpStream&a1=proto_v2&c1=restart"];
        for (int i = 0; i < 3; ++i) {
            [socket sendData:wolPacket toHost:@"10.5.5.255" port:9 withTimeout:-1 tag:0];
        }
        NSURLSessionDataTask *task = [session dataTaskWithURL:restartURL];
        [task resume];
        [openCV startWithPath:@"udp://@10.5.5.9:8554"];
    });
}

- (void)openCVDidFailToConnect:(OpenCV *)cv
{
    [self connect];
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotConnect:(NSError *)error
{
    printf("%s\n", [[error localizedDescription] UTF8String]);
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error
{
    printf("%s\n", [[error localizedDescription] UTF8String]);
}

- (void)udpSocketDidClose:(GCDAsyncUdpSocket *)sock withError:(NSError *)error
{
    printf("%s\n", [[error localizedDescription] UTF8String]);
}

@end
