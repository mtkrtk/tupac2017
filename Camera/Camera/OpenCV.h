#ifdef __cplusplus
#import <opencv/cv.hpp>
#endif
#import <Foundation/Foundation.h>

@interface OpenCVFrame : NSObject

- (instancetype)initWithData:(NSData *)data;

- (OpenCVFrame *)grayScale;
- (NSImage *)image;
- (void)drawLineFrom:(NSPoint)from to:(NSPoint)to;
- (NSData *)rawData;
- (NSSize)size;
- (void)encloseWithPoints:(NSPoint *)points numberOfPoints:(int)num;
#ifdef __cplusplus
- (cv::Mat *)mat;
- (instancetype)initWithMat:(cv::Mat *)mat;
#endif

@end

@class OpenCV;

@protocol OpenCVDelegate
@required

- (void)openCVDidConnectToSource:(OpenCV *)cv;
- (void)openCVDidFailToConnect:(OpenCV *)cv;
- (void)openCV:(OpenCV *)cv didCaptureFrame:(OpenCVFrame *)frame;

@end

@interface OpenCV : NSObject

- (instancetype)initWithDelegate:(id <OpenCVDelegate>)delegate;
- (void)startWithPath:(NSString *)path;

@end
