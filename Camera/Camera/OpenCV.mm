#import "OpenCV.h"
#import <Cocoa/Cocoa.h>

@implementation OpenCVFrame
{
    cv::Mat *frame;
}

- (instancetype)initWithData:(NSData *)data
{
    if (self = [super init]) {
        std::vector<uchar> bytes;
        bytes.assign(reinterpret_cast<uchar *>(const_cast<void *>(data.bytes)), reinterpret_cast<uchar *>(const_cast<void *>(data.bytes)) + data.length);
        frame = new cv::Mat();
        *frame = cv::imdecode(bytes, cv::IMREAD_COLOR);
    }
    return self;
}

- (instancetype)initWithMat:(cv::Mat *)mat
{
    if (self = [super init]) {
        frame = mat;
    }
    return self;
}

- (void)dealloc
{
    delete frame;
}

- (OpenCVFrame *)grayScale
{
    cv::Mat *gray = new cv::Mat();
    cvtColor(*frame, *gray, CV_BGR2GRAY);
    return [[OpenCVFrame alloc] initWithMat:gray];
}

- (NSImage *)image
{
    int width = frame->cols;
    int height = frame->rows;
    CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
    CGBitmapInfo info = kCGImageAlphaNone | kCGBitmapByteOrderDefault;
    cvtColor(*frame, *frame, CV_BGR2RGB);
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)[self rawData]);
    CGImageRef cgImage = CGImageCreate(width, height, 8, 8 * frame->elemSize(),
                                       frame->step[0], space, info, provider,
                                       nil, false, kCGRenderingIntentDefault);
    NSImage *image = [[NSImage alloc] initWithCGImage:cgImage
                                                 size:NSZeroSize];
    CGImageRelease(cgImage);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(space);
    return image;
}

- (void)drawLineFrom:(NSPoint)from to:(NSPoint)to
{
    line(*frame, cv::Point2f(from.x, from.y), cv::Point2f(from.x, from.y), cvScalar(255, 0, 0), 3);
}

- (NSData *)rawData
{
    return [NSData dataWithBytesNoCopy:frame->data
                                length:frame->elemSize() * frame->total()
                          freeWhenDone:NO];
}

- (NSSize)size
{
    return NSMakeSize(frame->cols, frame->rows);
}

- (void)encloseWithPoints:(NSPoint *)points numberOfPoints:(int)num
{
    std::vector<cv::Point> vp;
    for (int i = 0; i < num; ++i){
        vp.push_back(cv::Point(points[i].x, points[i].y));
    }
    cv::RotatedRect r = cv::minAreaRect(vp);
    cv::Point2f pts[4];
    r.points(pts);
    for (int i = 0; i < 4; ++i){
        line(*frame, pts[i], pts[(i+1)%4], cvScalar(255, 0, 0), 3);
    }
}

- (cv::Mat *)mat
{
    return frame;
}

@end

@implementation OpenCV
{
    id <OpenCVDelegate> delegate;
    cv::VideoCapture *capture;
}

- (instancetype)init
{
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

- (instancetype)initWithDelegate:(id <OpenCVDelegate>)aDelegate
{
    if (self = [super init]) {
        delegate = aDelegate;
    }
    return self;
}

- (void)startWithPath:(NSString *)path
{
    if (path) {
        capture = new cv::VideoCapture([path UTF8String], cv::CAP_FFMPEG);
    } else {
        capture = new cv::VideoCapture(0);
    }
    
    if (! capture->isOpened()) {
        delete capture;
        [delegate openCVDidFailToConnect:self];
        return;
    }
    
    [delegate openCVDidConnectToSource:self];
    
    while (1) {
        cv::Mat *frame = new cv::Mat();
        while (! capture->read(*frame)) ;
        OpenCVFrame *cvFrame = [[OpenCVFrame alloc] initWithMat:frame];
        [delegate openCV:self didCaptureFrame:cvFrame];
    }
}

@end
