#import <Cocoa/Cocoa.h>
#import "GCDAsyncUdpSocket.h"

@interface AppDelegate : NSObject <NSApplicationDelegate, GCDAsyncUdpSocketDelegate>

@property (weak) IBOutlet NSImageView *imageView;

- (IBAction)resetInitialFrame:(NSButton *)sender;

@end

