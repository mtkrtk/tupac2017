#import "MotionDetector.h"
#import "AppDelegate.h"
#import "NSImage+flip.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate
{
    OpenCVFrame *currentFrame;
    MotionDetector *detector;
    GCDAsyncUdpSocket *socket;
    dispatch_queue_t socketDelegateQueue;
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data
      fromAddress:(NSData *)address withFilterContext:(id)filterContext
{
    NSImage *image = [[[NSImage alloc] initWithData:data] flip];
    OpenCVFrame *frame = [[OpenCVFrame alloc] initWithData:image.TIFFRepresentation];
    [detector detectAndEnclose:frame];
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self.imageView setImage:[frame image]];
        currentFrame = frame;
    });
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    detector = [[MotionDetector alloc] init];
    
    socketDelegateQueue = dispatch_queue_create("SocketDelegateQUeue", DISPATCH_QUEUE_SERIAL);
    socket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:socketDelegateQueue];
    socket.maxReceiveIPv4BufferSize = UINT16_MAX;
    
    NSError *error = nil;
    [socket bindToPort:49153 error:&error];
    if (error) {
        NSLog(@"%@", error.localizedDescription);
        return;
    }
    
    [socket beginReceiving:&error];
    if (error) {
        NSLog(@"%@", error.localizedDescription);
        return;
    }
}

- (IBAction)resetInitialFrame:(NSButton *)sender
{
    [detector setInitialFrame:currentFrame];
}

@end
