#import "OpenCV.h"
#import <Foundation/Foundation.h>

@interface MotionDetector : NSObject

- (instancetype)init;
- (void)detectAndEnclose:(OpenCVFrame *)frame;
- (void)setInitialFrame:(OpenCVFrame *)frame;

@end
