#import <opencv/cv.hpp>
#import "MotionDetector.h"

@implementation MotionDetector
{
    cv::Mat initialFrame;
}

- (instancetype)init
{
    if (self = [super init]) {
        
    }
    return self;
}

- (void)detectAndEnclose:(OpenCVFrame *)aFrame
{
    cv::Mat *frame = [aFrame mat];
    cv::Mat gray;
    cv::cvtColor(*frame, gray, cv::COLOR_BGR2GRAY);
    cv::GaussianBlur(gray, gray, cv::Size(21, 21), 0);
    
    if (initialFrame.empty()) {
        gray.copyTo(initialFrame);
        return;
    }
    
    cv::Mat frameDelta;
    cv::absdiff(initialFrame, gray, frameDelta);
    cv::Mat thresh;
    cv::threshold(frameDelta, thresh, 40, 255, cv::THRESH_BINARY);
    
    cv::dilate(thresh, thresh, cv::Mat(), cv::Point(-1, -1), 2);
    cv::Mat threshCopy = thresh;
    std::vector<std::vector<cv::Point>> conts;
    cv::findContours(threshCopy, conts, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
    
    for (auto &&c : conts) {
        if (cv::contourArea(c) < 100 || cv::contourArea(c) > 500) {
            continue;
        }
        cv::Rect rect = cv::boundingRect(c);
        cv::rectangle(*frame, cv::Point(rect.x, rect.y),
                      cv::Point(rect.x + rect.width, rect.y + rect.height),
                      cv::Scalar(0, 255, 255), 2);
    }
}

- (void)setInitialFrame:(OpenCVFrame *)aFrame
{
    cv::Mat *frame = [aFrame mat];
    cv::Mat gray;
    cv::cvtColor(*frame, gray, cv::COLOR_BGR2GRAY);
    cv::GaussianBlur(gray, gray, cv::Size(21, 21), 0);
    gray.copyTo(initialFrame);
}

@end
