#import <Cocoa/Cocoa.h>
#import "GCDAsyncUdpSocket.h"

@interface AppDelegate : NSObject <NSApplicationDelegate, GCDAsyncUdpSocketDelegate>

@property (weak) IBOutlet NSTextField *label;
@property (weak) IBOutlet NSImageView *imageView;

@end

