#import "AppDelegate.h"
#import <CocoaAsyncSocket/GCDAsyncUdpSocket.h>
#import "ZBar.h"
#import "OpenCV.h"
#import "NSImage+flip.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate
{
    OpenCVFrame *currentFrame;
    ZBar *zbar;
    GCDAsyncUdpSocket *socket;
    dispatch_queue_t socketDelegateQueue;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    zbar = [[ZBar alloc] init];
    
    socketDelegateQueue = dispatch_queue_create("SocketDelegateQUeue", DISPATCH_QUEUE_SERIAL);
    socket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:socketDelegateQueue];
    socket.maxReceiveIPv4BufferSize = UINT16_MAX;
    
    NSError *error = nil;
    [socket bindToPort:49153 error:&error];
    if (error) {
        NSLog(@"%@", error.localizedDescription);
        return;
    }
    
    [socket beginReceiving:&error];
    if (error) {
        NSLog(@"%@", error.localizedDescription);
        return;
    }
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data
      fromAddress:(NSData *)address withFilterContext:(id)filterContext
{
    NSImage *image = [[[NSImage alloc] initWithData:data] flip];
    OpenCVFrame *frame = [[OpenCVFrame alloc] initWithData:image.TIFFRepresentation];
    OpenCVFrame *gray = [frame grayScale];
    NSArray<ZBarScanResult *> *scans = [zbar scanWithData:gray.rawData imageSize:gray.size];
    if (scans.count > 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.label setStringValue:scans[0].stringValue];
        });
        NSPoint *points;
        int numOfPoints;
        for (int i = 0; i < scans.count; ++i) {
            [scans[i] getPoints:&points numberOfPoints:&numOfPoints];
            [frame encloseWithPoints:points numberOfPoints:numOfPoints];
        }
//        [scans[0] getPoints:&points numberOfPoints:&numOfPoints];
//        [frame encloseWithPoints:points numberOfPoints:numOfPoints];
    }
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self.imageView setImage:[frame image]];
        currentFrame = frame;
    });
}

@end
