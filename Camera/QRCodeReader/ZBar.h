#import <Foundation/Foundation.h>

@interface ZBarScanResult : NSObject

- (NSString *)stringValue;
- (void)getPoints:(NSPoint **)points numberOfPoints:(int *)num;

@end

@interface ZBar : NSObject

- (instancetype)init;
- (NSArray<ZBarScanResult *> *)scanWithData:(NSData *)data imageSize:(NSSize)size;

@end
