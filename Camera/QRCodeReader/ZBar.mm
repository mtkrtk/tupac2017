#import <zbar.h>
#import "ZBar.h"

@implementation ZBarScanResult
{
    NSString *value;
    NSPoint *points;
    int numOfPoints;
}

- (instancetype)init
{
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

- (instancetype)initWithSymbol:(zbar::Image::SymbolIterator)symbol
{
    if (self = [super init]) {
        value = [NSString stringWithUTF8String:symbol->get_data().c_str()];
        numOfPoints = symbol->get_location_size();
        points = (NSPoint *)malloc(sizeof(NSPoint) * numOfPoints);
        for (int i = 0; i < numOfPoints; ++i) {
            points[i].x = symbol->get_location_x(i);
            points[i].y = symbol->get_location_y(i);
        }
    }
    return self;
}

- (void)dealloc
{
    free(points);
}

- (NSString *)stringValue
{
    return value;
}

- (void)getPoints:(NSPoint **)pointsP numberOfPoints:(int *)num
{
    *pointsP = points;
    *num = numOfPoints;
}

@end

@implementation ZBar
{
    zbar::ImageScanner scanner;
}

- (instancetype)init
{
    if (self = [super init]) {
        scanner.set_config(zbar::ZBAR_NONE, zbar::ZBAR_CFG_ENABLE, 1);
    }
    return self;
}

- (NSArray<ZBarScanResult *> *)scanWithData:(NSData *)data imageSize:(NSSize)size
{
    int width  = size.width;
    int height = size.height;
    zbar::Image image(width, height, "Y800", [data bytes], [data length]);
    
    int status = scanner.scan(image);
    if (status < 0) {
        printf("scan error\n");
    }
    NSMutableArray *results = [[NSMutableArray alloc] initWithCapacity:status];
    for (zbar::Image::SymbolIterator symbol = image.symbol_begin();
         symbol != image.symbol_end();
         ++symbol) {
        [results addObject:[[ZBarScanResult alloc] initWithSymbol:symbol]];
    }
    return results;
}

@end
