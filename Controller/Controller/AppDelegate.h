#import <Cocoa/Cocoa.h>
#import "RDTP.h"

@interface AppDelegate : NSObject <NSApplicationDelegate, RDTPDelegate>

@property (weak) IBOutlet NSSlider *leftXSlider;
@property (weak) IBOutlet NSSlider *leftYSlider;
@property (weak) IBOutlet NSSlider *rightXSlider;
@property (weak) IBOutlet NSSlider *rightYSlider;
@property (weak) IBOutlet NSTextField *leftXLabel;
@property (weak) IBOutlet NSTextField *leftYLabel;
@property (weak) IBOutlet NSTextField *rightXLabel;
@property (weak) IBOutlet NSTextField *rightYLabel;
@property (weak) IBOutlet NSSlider *leftTriggerSlider;
@property (weak) IBOutlet NSSlider *rightTriggerSlider;
@property (weak) IBOutlet NSTextField *leftTriggerLabel;
@property (weak) IBOutlet NSTextField *rightTriggerLabel;
@property (weak) IBOutlet NSSlider *leftMotorSlider;
@property (weak) IBOutlet NSSlider *rightMotorSlider;
@property (weak) IBOutlet NSTextField *leftMotorLabel;
@property (weak) IBOutlet NSTextField *rightMotorLabel;
@property (weak) IBOutlet NSButton *robotFoundCheck;
@property (weak) IBOutlet NSImageView *videoView;
@property (weak) IBOutlet NSImageView *leftFrontBar;
@property (weak) IBOutlet NSImageView *leftBackBar;
@property (weak) IBOutlet NSImageView *rightFrontBar;
@property (weak) IBOutlet NSImageView *rightBackBar;
@property (weak) IBOutlet NSImageView *arm1;
@property (weak) IBOutlet NSImageView *arm2;
@property (weak) IBOutlet NSImageView *arm3;
@property (weak) IBOutlet NSImageView *arm4;

- (IBAction)toggleArm1:(NSButton *)sender;
- (IBAction)toggleArm2:(NSButton *)sender;
- (IBAction)sayHello:(NSButton *)sender;

@end

