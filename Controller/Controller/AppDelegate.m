#import "AppDelegate.h"
#import "GamepadController.h"
#import "WiredXBoxController.h"
#import "Aspects.h"
#import "RDTPPacket.h"
#import "StickConverter.h"
#import <Quartz/Quartz.h>
#import "NSImage+flip.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate
{
    Transport *transport;
    RDTP *app;
    GamepadController *controller;
    dispatch_queue_t mainQueue;
    StickConverter *converter;
    RDTPPacket packet;
    BOOL shouldFlip;
    NSData *lastPacket;
    
    CABasicAnimation *leftFrontAnimation;
    CABasicAnimation *leftBackAnimation;
    CABasicAnimation *rightFrontAnimation;
    CABasicAnimation *rightBackAnimation;
    CABasicAnimation *arm1Animation;
    CABasicAnimation *arm2Animation;
    CABasicAnimation *arm3Animation;
    CABasicAnimation *arm4Animation;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    shouldFlip = NO;
    lastPacket = nil;
    transport = [[Transport alloc] init];
    controller = [GamepadController controller];
    controller.application = app;
    BOOL isControllerWired = NO;
    if ([controller class] == [WiredXBoxController class]) {
        isControllerWired = YES;
    }
    converter = [[StickConverter alloc] initWithRDTPPacket:&packet];
    converter.shouldFlip = shouldFlip;
    controller.delegate = converter;
    mainQueue = dispatch_get_main_queue();
    __block AppDelegate *blockSelf = self;
    if (controller) {
        [converter aspect_hookSelector:@selector(gamepad:updateValue:forStickAxis:)
                     withOptions:AspectPositionAfter
                      usingBlock:^(id <AspectInfo> info, GamepadController *gamepad, int value, GamepadStickAxis axis) {
                          NSString *valueString = [NSString stringWithFormat:@"%d", value];
                          switch (axis) {
                              case LeftStickX:
                                  blockSelf.leftXSlider.integerValue = value;
                                  blockSelf.leftXLabel.stringValue = valueString;
                                  break;
                                  
                              case LeftStickY:
                                  blockSelf.leftYSlider.integerValue = value;
                                  blockSelf.leftYLabel.stringValue = valueString;
                                  break;
                                  
                              case RightStickX:
                                  blockSelf.rightXSlider.integerValue = value;
                                  blockSelf.rightXLabel.stringValue = valueString;
                                  break;
                                  
                              case RightStickY:
                                  blockSelf.rightYSlider.integerValue = value;
                                  blockSelf.rightYLabel.stringValue = valueString;
                                  break;
                                  
                              case LeftTrigger:
                                  blockSelf.leftTriggerSlider.integerValue = value;
                                  blockSelf.leftTriggerLabel.stringValue = valueString;
//                                  if (isControllerWired) {
//                                      ((WiredXBoxController *)controller).largeMotorPower = value / (2.0 * GAMEPAD_MAX) + 0.5;
//                                  }
                                  break;
                                  
                              case RightTrigger:
                                  blockSelf.rightTriggerSlider.integerValue = value;
                                  blockSelf.rightTriggerLabel.stringValue = valueString;
//                                  if (isControllerWired) {
//                                      ((WiredXBoxController *)controller).smallMotorPower = value / (2.0 * GAMEPAD_MAX) + 0.5;
//                                  }
                                  break;
                                  
                              default:
                                  break;
                          }
                      }
                           error:nil];
    }
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserverForName:RDTPRobotDidFoundNotification object:app queue:nil
                    usingBlock:^(NSNotification * _Nonnull note) {
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            blockSelf.robotFoundCheck.state = NSOnState;
                        });
                    }
     ];
    RDTPPacket_init(&packet);
    app = [[RDTP alloc] initWithTransport:transport];
    app.delegate = self;
    
    leftFrontAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    leftFrontAnimation.removedOnCompletion = NO;
    leftFrontAnimation.fillMode = kCAFillModeForwards;
    leftBackAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    leftBackAnimation.removedOnCompletion = NO;
    leftBackAnimation.fillMode = kCAFillModeForwards;
    rightFrontAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rightFrontAnimation.removedOnCompletion = NO;
    rightFrontAnimation.fillMode = kCAFillModeForwards;
    rightBackAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rightBackAnimation.removedOnCompletion = NO;
    rightBackAnimation.fillMode = kCAFillModeForwards;
    
    arm1Animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    arm1Animation.removedOnCompletion = NO;
    arm1Animation.fillMode = kCAFillModeForwards;
    arm2Animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    arm2Animation.removedOnCompletion = NO;
    arm2Animation.fillMode = kCAFillModeForwards;
    arm3Animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    arm3Animation.removedOnCompletion = NO;
    arm3Animation.fillMode = kCAFillModeForwards;
    arm4Animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    arm4Animation.removedOnCompletion = NO;
    arm4Animation.fillMode = kCAFillModeForwards;
}

- (IBAction)startVideo:(NSButton *)sender
{
    RDTPPacket_setCommand(&packet, StartVideo);
}

- (IBAction)stopVideo:(NSButton *)sender
{
    RDTPPacket_setCommand(&packet, StopVideo);
}

- (IBAction)flip:(NSButton *)sender
{
    shouldFlip = ! shouldFlip;
    converter.shouldFlip = shouldFlip;
}

- (void)RDTP:(RDTP *)app videoFrameAvailable:(NSData *)jpg
{
    dispatch_async(mainQueue, ^{
        NSImage *image = [[NSImage alloc] initWithData:jpg];
        if (! shouldFlip) {
            image = [image flip];
        }
        self.videoView.image = image;
    });
}

- (NSData *)RDTPWillSendPacket:(RDTP *)app
{
    NSData *data = [converter makePacketData];
//    if (data.length == 0) {
//        data = lastPacket;
//    } else {
//        lastPacket = data;
//    }
    
    if (! data) {
        return nil;
    }
    RDTPPacket aPacket;
    RDTPPacket_initWithBytes(&aPacket, (int8_t *)data.bytes, (int)data.length);
    int8_t value;
    RDTPPacketComponent component;
    while (RDTPPacket_getReceiveData(&aPacket, &value, &component) == DataAvailable) {
        switch (component) {
            case LeftMotor:
                self.leftMotorSlider.integerValue = value;
                self.leftMotorLabel.stringValue = [NSString stringWithFormat:@"%d", value];
                break;
                
            case RightMotor:
                self.rightMotorSlider.integerValue = value;
                self.rightMotorLabel.stringValue = [NSString stringWithFormat:@"%d", value];
                break;
                
            case Servo6:
                [leftFrontAnimation setToValue: [NSNumber numberWithFloat:value / 127.0 * M_PI]];
                leftFrontAnimation.repeatCount = 0;
                leftFrontAnimation.duration = 0;
                leftFrontAnimation.beginTime = 0;
                leftFrontAnimation.cumulative = YES;
                leftFrontAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
                [self.leftFrontBar.layer addAnimation:leftFrontAnimation forKey:@"rotateAnimation"];
                break;
                
            case Servo3:
                [rightFrontAnimation setToValue: [NSNumber numberWithFloat:-value / 127.0 * M_PI]];
                rightFrontAnimation.repeatCount = 0;
                rightFrontAnimation.duration = 0;
                rightFrontAnimation.beginTime = 0;
                rightFrontAnimation.cumulative = YES;
                rightFrontAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
                [self.rightFrontBar.layer addAnimation:rightFrontAnimation forKey:@"rotateAnimation"];
                break;
                
            case Servo2:
                [leftBackAnimation setToValue: [NSNumber numberWithFloat:-value / 127.0 * M_PI]];
                leftBackAnimation.repeatCount = 0;
                leftBackAnimation.duration = 0;
                leftBackAnimation.beginTime = 0;
                leftBackAnimation.cumulative = YES;
                leftBackAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
                [self.leftBackBar.layer addAnimation:leftBackAnimation forKey:@"rotateAnimation"];
                break;
                
            case Servo0:
                [rightBackAnimation setToValue: [NSNumber numberWithFloat:value / 127.0 * M_PI]];
                rightBackAnimation.repeatCount = 0;
                rightBackAnimation.duration = 0;
                rightBackAnimation.beginTime = 0;
                rightBackAnimation.cumulative = YES;
                rightBackAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
                [self.rightBackBar.layer addAnimation:rightBackAnimation forKey:@"rotateAnimation"];
                break;
                
            case Servo4:
                [arm1Animation setToValue: [NSNumber numberWithFloat:value / 127.0 * M_PI]];
                arm1Animation.repeatCount = 0;
                arm1Animation.duration = 0;
                arm1Animation.beginTime = 0;
                arm1Animation.cumulative = YES;
                arm1Animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
                [self.arm1.layer addAnimation:arm1Animation forKey:@"rotateAnimation"];
                break;
                
            case Servo9:
                [arm2Animation setToValue: [NSNumber numberWithFloat:value / 127.0 * M_PI]];
                arm2Animation.repeatCount = 0;
                arm2Animation.duration = 0;
                arm2Animation.beginTime = 0;
                arm2Animation.cumulative = YES;
                arm2Animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
                [self.arm2.layer addAnimation:arm2Animation forKey:@"rotateAnimation"];
                break;
                
            case Servo5:
                [arm3Animation setToValue: [NSNumber numberWithFloat:value / 127.0 * M_PI]];
                arm3Animation.repeatCount = 0;
                arm3Animation.duration = 0;
                arm3Animation.beginTime = 0;
                arm3Animation.cumulative = YES;
                arm3Animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
                [self.arm3.layer addAnimation:arm3Animation forKey:@"rotateAnimation"];
                break;
                
            case Servo1:
                [arm4Animation setToValue: [NSNumber numberWithFloat:value / 127.0 * M_PI]];
                arm4Animation.repeatCount = 0;
                arm4Animation.duration = 0;
                arm4Animation.beginTime = 0;
                arm4Animation.cumulative = YES;
                arm4Animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
                [self.arm4.layer addAnimation:arm4Animation forKey:@"rotateAnimation"];
                break;
                
            default:
                break;
        }
    }
    
    return data;
}

- (IBAction)toggleArm1:(NSButton *)sender
{
    if (sender.state == NSOnState) {
        RDTPPacket_updateValue(&packet, EnableServo, 4);
    } else {
        RDTPPacket_updateValue(&packet, DisableServo, 4);
    }
}

- (IBAction)toggleArm2:(NSButton *)sender
{
    if (sender.state == NSOnState) {
        RDTPPacket_updateValue(&packet, EnableServo, 9);
    } else {
        RDTPPacket_updateValue(&packet, DisableServo, 9);
    }
}

- (IBAction)sayHello:(NSButton *)sender
{
    RDTPPacket_setCommand(&packet, SayHello);
}

@end
