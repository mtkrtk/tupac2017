#import <Cocoa/Cocoa.h>

@interface NSImage (flip)

- (NSImage *)flip;

@end
