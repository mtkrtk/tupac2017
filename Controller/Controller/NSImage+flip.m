#import "NSImage+flip.h"

@implementation NSImage (flip)

- (NSImage *)flip
{
    if (self.size.height == 0) {
        return nil;
    }
    NSRect imageBounds = NSZeroRect;
    imageBounds.size = self.size;
    NSBezierPath *pathBounds = [NSBezierPath bezierPathWithRect:imageBounds];
    NSAffineTransform *transform = [NSAffineTransform transform];
    [transform rotateByDegrees:180.0];
    [pathBounds transformUsingAffineTransform:transform];
    NSRect rotatedBounds = NSMakeRect(NSZeroPoint.x, NSZeroPoint.y, self.size.width, self.size.height);
    NSImage *rotatedImage = [[NSImage alloc] initWithSize:rotatedBounds.size];
    
    imageBounds.origin.x = NSMidX(rotatedBounds) - (NSWidth(imageBounds) / 2);
    imageBounds.origin.y = NSMidY(rotatedBounds) - (NSHeight(imageBounds) / 2);
    
    transform = [NSAffineTransform transform];
    [transform translateXBy:NSWidth(rotatedBounds) / 2 yBy:NSHeight(rotatedBounds) / 2];
    [transform rotateByDegrees:180.0];
    [transform translateXBy:-NSWidth(rotatedBounds) / 2 yBy:-NSHeight(rotatedBounds) / 2];
    [rotatedImage lockFocus];
    [transform concat];
    [self drawInRect:imageBounds fromRect:NSZeroRect operation:NSCompositeCopy fraction:1.0];
    [rotatedImage unlockFocus];
    
    return rotatedImage;
}

@end
