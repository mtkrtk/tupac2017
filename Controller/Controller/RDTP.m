#import "RDTP.h"
#import "StickConverter.h"
#include <netinet/in.h>
#import "ServoLimitter.h"

const NSNotificationName RDTPRobotDidFoundNotification = @"RDTPRobotDidFoundNotification";

@implementation RDTP
{
    Transport *transport;
    NSData *remoteAddress;
    NSTimer *sendTimer;
    StickConverter *converter;
    double lastLeftTriggerValue;
    double lastRightTriggerValue;
    double lastLeftXValue;
}

- (instancetype)initWithTransport:(Transport *)aTransport
{
    if (self = [super init]) {
        converter = [[StickConverter alloc] init];
        transport = aTransport;
        transport.delegate = self;
        [transport openWithPort:RDTP_PORT];
        lastLeftTriggerValue = 0;
        lastRightTriggerValue = 0;
        lastLeftXValue = 0;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(robotDidFoundCallback:)
                                                     name:RDTPRobotDidFoundNotification
                                                   object:self];
//        sendTimer = [NSTimer scheduledTimerWithTimeInterval:0.01
//                                                     target:self
//                                                   selector:@selector(sendData:)
//                                                   userInfo:nil
//                                                    repeats:YES];
    }
    return self;
}

- (void)robotDidFoundCallback:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    dispatch_async(dispatch_get_main_queue(), ^{
        sendTimer = [NSTimer scheduledTimerWithTimeInterval:0.01
                                                     target:self
                                                   selector:@selector(sendData:)
                                                   userInfo:nil
                                                    repeats:YES];
    });
}

- (void)sendData:(NSTimer *)timer
{
    @synchronized (self) {
        NSData *packet = [self.delegate RDTPWillSendPacket:self];
        if (packet && packet.length && remoteAddress) {
            [transport sendData:packet toAddress:remoteAddress];
        }
    }
}

- (void)transport:(Transport *)transport didReceiveData:(NSData *)data fromAddress:(NSData *)addressData
{
    NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    if ([string isEqualToString:@"This is a robot\n"]) {
        struct sockaddr_in address;
        memcpy(&address, [addressData bytes], sizeof(struct sockaddr_in));
        address.sin_port = htons(RDTP_PORT);
        
        remoteAddress = [NSData dataWithBytes:&address length:sizeof(struct sockaddr_in)];
        [[NSNotificationCenter defaultCenter] postNotificationName:RDTPRobotDidFoundNotification
                                                            object:self];
    } else {
        [self.delegate RDTP:self videoFrameAvailable:data];
    }
}

@end
