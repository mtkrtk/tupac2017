#import "StickConverter.h"
#import "GamepadController.h"
#import "ServoLimitter.h"

#define scale(val, sourceMax, destMax) ((destMax) * (val) / (sourceMax))
static const int MediumMaxPower = 76;
static const int BurstMaxPower = 127;
static const int SmallMaxPower = 38;

@implementation StickConverter
{
    int lastLeftTriggerValue;
    int lastLeftXValue;
    int max;
    RDTPPacketComponent servo;
    RDTPPacket *packet;
    NSArray<ServoLimitter *> *limitters;
    int maxPower;
}

- (instancetype)initWithRDTPPacket:(RDTPPacket *)aPacket
{
    if (self = [super init]) {
        lastLeftTriggerValue = -65536;
        lastLeftXValue = 0;
        max = 0;
        maxPower = MediumMaxPower;
        servo = 1 << Servo1;
        packet = aPacket;
        RDTPPacket_init(packet);
        
        NSMutableArray<ServoLimitter *> *mutableLimitters = [NSMutableArray new];
        for (RDTPPacketComponent i = Servo0; i <= Servo9; ++i) {
            [mutableLimitters addObject:[[ServoLimitter alloc] initWithServo:i packet:packet]];
        }
        mutableLimitters[4].currentValue = INT8_MIN;
        mutableLimitters[9].currentValue = INT8_MIN;
        limitters = mutableLimitters;
        limitters[Servo6 - Servo0].shouldInvert = YES;
        limitters[Servo2 - Servo0].shouldInvert = YES;
        limitters[Servo9 - Servo0].lowerLimit = INT8_MIN;
        limitters[Servo4 - Servo0].upperLimit = INT8_MAX;
        limitters[Servo4 - Servo0].lowerLimit = INT8_MIN;
        limitters[Servo1 - Servo0].upperLimit = INT8_MAX;
        limitters[Servo5 - Servo0].lowerLimit = INT8_MIN;
        limitters[Servo5 - Servo0].upperLimit = INT8_MAX;
    }
    return self;
}

- (void)gamepad:(GamepadController *)gamepad updateValue:(int)value forStickAxis:(GamepadStickAxis)stick
{
    switch (stick) {
        case XButton:
            if (self.shouldFlip) {
                /* right back sub-crawler */
                if (value) {
                    servo |= 1 << Servo0;
                } else {
                    servo &= ~(1 << Servo0);
                    [limitters[0] updateStep:0];
                }
            } else {
                /* left front sub-crawler */
                if (value) {
                    servo |= 1 << Servo6;
                } else {
                    servo &= ~(1 << Servo6);
                    [limitters[6] updateStep:0];
                }
            }
            return;
            
        case YButton:
            if (self.shouldFlip) {
                /* left back sub-crawler */
                if (value) {
                    servo |= 1 << Servo2;
                } else {
                    servo &= ~(1 << Servo2);
                    [limitters[2] updateStep:0];
                }
            } else {
                /* right front sub-crawler */
                if (value) {
                    servo |= 1 << Servo3;
                } else {
                    servo &= ~(1 << Servo3);
                    [limitters[8] updateStep:0];
                }
            }
            return;
            
        case AButton:
            if (self.shouldFlip) {
                /* right front sub-crawler */
                if (value) {
                    servo |= 1 << Servo3;
                } else {
                    servo &= ~(1 << Servo3);
                    [limitters[8] updateStep:0];
                }
            } else {
                /* left back sub-crawler */
                if (value) {
                    servo |= 1 << Servo2;
                } else {
                    servo &= ~(1 << Servo2);
                    [limitters[2] updateStep:0];
                }
            }
            return;
            
        case BButton:
            if (self.shouldFlip) {
                /* left front sub-crawler */
                if (value) {
                    servo |= 1 << Servo6;
                } else {
                    servo &= ~(1 << Servo6);
                    [limitters[6] updateStep:0];
                }
            } else {
                /* right back sub-crawler */
                if (value) {
                    servo |= 1 << Servo0;
                } else {
                    servo &= ~(1 << Servo0);
                    [limitters[0] updateStep:0];
                }
            }
            return;
            
        case Pov:
            switch (value) {
                case -1:
                    servo |= 1 << Servo1;
                    servo &= ~(1 << Servo9 | 1 << Servo4 | 1 << Servo5 | 1 << Servo7);
                    [limitters[9] updateStep:0];
                    [limitters[4] updateStep:0];
                    [limitters[5] updateStep:0];
                    [limitters[7] updateStep:0];
                    break;
                    
                case 0:
                    servo |= 1 << Servo9;
                    servo &= ~(1 << Servo1 | 1 << Servo4 | 1 << Servo5 | 1 << Servo7);
                    [limitters[1] updateStep:0];
                    [limitters[4] updateStep:0];
                    [limitters[5] updateStep:0];
                    [limitters[7] updateStep:0];
                    break;
                    
                case 9000:
                    servo |= 1 << Servo4;
                    servo &= ~(1 << Servo1 | 1 << Servo9 | 1 << Servo5 | 1 << Servo7);
                    [limitters[1] updateStep:0];
                    [limitters[9] updateStep:0];
                    [limitters[5] updateStep:0];
                    [limitters[7] updateStep:0];
                    break;
                    
                case 18000:
                    servo |= 1 << Servo7;
                    servo &= ~(1 << Servo1 | 1 << Servo9 | 1 << Servo4 | 1 << Servo7);
                    [limitters[1] updateStep:0];
                    [limitters[9] updateStep:0];
                    [limitters[4] updateStep:0];
                    [limitters[5] updateStep:0];
                    break;
                    
                case 27000:
                    servo |= 1 << Servo5;
                    servo &= ~(1 << Servo1 | 1 << Servo9 | 1 << Servo4 | 1 << Servo7);
                    [limitters[1] updateStep:0];
                    [limitters[9] updateStep:0];
                    [limitters[4] updateStep:0];
                    [limitters[7] updateStep:0];
                    break;
                    
                default:
                    break;
            }
            return;
            
        case RightStickY:
        {
            int8_t angle = scale(value, GAMEPAD_MAX, INT8_MAX);
            const RDTPPacketComponent servos[] = {Servo1, Servo9, Servo4, Servo5, Servo7};
            for (int i = 0; i < sizeof(servos) / sizeof(servos[0]); ++i) {
                if (servo & (1 << servos[i])) {
                    [limitters[servos[i] - Servo0] updateStep:angle];
                }
            }
            return;
        }
            
        case LeftStickButton:
            if (value) {
                const RDTPPacketComponent servos[] = {Servo0, Servo2, Servo6, Servo3};
                for (int i = 0; i < sizeof(servos) / sizeof(servos[0]); ++i) {
                    [limitters[servos[i] - Servo0] preset];
                }
            }
            return;

        case LeftStickY:
        {
            int8_t angle = scale(value, GAMEPAD_MAX, INT8_MAX);
            const RDTPPacketComponent servos[] = {Servo0, Servo2, Servo6, Servo3};
            for (int i = 0; i < sizeof(servos) / sizeof(servos[0]); ++i) {
                if (servo & (1 << servos[i])) {
                    [limitters[servos[i] - Servo0] updateStep:angle];
                }
            }
            return;
        }
            
        case RightStickButton:
            if (value) {
                servo &= ~(1 << Servo1);
                if (self.shouldFlip) {
                    [limitters[1] updateStep:INT8_MAX];
                    [limitters[9] updateStep:INT8_MAX];
                    [limitters[4] updateStep:INT8_MIN];
                    [limitters[7] preset];
                    [limitters[5] preset];
                } else {
                    [limitters[1] preset];
                    [limitters[9] updateStep:INT8_MIN];
                    [limitters[4] updateStep:INT8_MIN];
                    [limitters[7] preset];
                    [limitters[5] preset];
                }
            } else {
                servo |= 1 << Servo1;
            }
            return;
            
        case LeftTrigger:
            if (self.shouldFlip) {
                max = (maxPower * (value + GAMEPAD_MAX)) / (2 * GAMEPAD_MAX);
            } else {
                max = (-maxPower * (value + GAMEPAD_MAX)) / (2 * GAMEPAD_MAX);
            }
            break;
            
        case RightTrigger:
            if (self.shouldFlip) {
                max = (-maxPower * (value + GAMEPAD_MAX)) / (2 * GAMEPAD_MAX);
            } else {
                max = (maxPower * (value + GAMEPAD_MAX)) / (2 * GAMEPAD_MAX);
            }
            break;
            
        case LeftStickX:
            if (self.shouldFlip) {
                lastLeftXValue = -value;
            } else {
                lastLeftXValue = value;
            }
            break;
            
        case RightTriggerButton:
//        case LeftTriggerButton:
            if (! self.shouldFlip) {
                if (value) {
                    RDTPPacket_updateValue(packet, LeftMotor, maxPower);
                    RDTPPacket_updateValue(packet, RightMotor, -maxPower);
                } else {
                    RDTPPacket_updateValue(packet, LeftMotor, 0);
                    RDTPPacket_updateValue(packet, RightMotor, 0);
                }
            } else if (value) {
                RDTPPacket_updateValue(packet, LeftMotor, -maxPower);
                RDTPPacket_updateValue(packet, RightMotor, maxPower);
            } else {
                RDTPPacket_updateValue(packet, LeftMotor, 0);
                RDTPPacket_updateValue(packet, RightMotor, 0);
            }
            return;
            
        case LeftTriggerButton:
//        case RightTriggerButton:
            if (! self.shouldFlip) {
                if (value) {
                    RDTPPacket_updateValue(packet, LeftMotor, -maxPower);
                    RDTPPacket_updateValue(packet, RightMotor, maxPower);
                } else {
                    RDTPPacket_updateValue(packet, LeftMotor, 0);
                    RDTPPacket_updateValue(packet, RightMotor, 0);
                }
            } else if (value) {
                RDTPPacket_updateValue(packet, LeftMotor, maxPower);
                RDTPPacket_updateValue(packet, RightMotor, -maxPower);
            } else {
                RDTPPacket_updateValue(packet, LeftMotor, 0);
                RDTPPacket_updateValue(packet, RightMotor, 0);
            }
            return;
            
        case XBoxButton:
            if (value) {
                if (maxPower == MediumMaxPower) {
                    maxPower = BurstMaxPower;
                } else {
                    maxPower = MediumMaxPower;
                }
            }
            return;
            
        case SelectButton:
            if (value) {
                [limitters[Servo0 - Servo0] updateStep:INT8_MAX];
                [limitters[Servo2 - Servo0] updateStep:INT8_MAX];
                [limitters[Servo6 - Servo0] updateStep:-INT8_MAX];
                [limitters[Servo3 - Servo0] updateStep:-INT8_MAX];
            }
            return;
            
        case StartButton:
            if (value) {
                if (maxPower == MediumMaxPower) {
                    maxPower = SmallMaxPower;
                } else {
                    maxPower = MediumMaxPower;
                }
            }
            return;
            
        default:
            return;
    }
    
    if (max) {
        int8_t rightMotor;
        int8_t leftMotor;
        if (lastLeftXValue > 0) {
            leftMotor = max;
            rightMotor = (max * (GAMEPAD_MAX - lastLeftXValue)) / GAMEPAD_MAX;
        } else if (lastLeftXValue < 0) {
            leftMotor = (max * (GAMEPAD_MAX + lastLeftXValue)) / GAMEPAD_MAX;
            rightMotor = max;
        } else {
            leftMotor = max;
            rightMotor = max;
        }
        RDTPPacket_updateValue(packet, LeftMotor, -leftMotor);
        RDTPPacket_updateValue(packet, RightMotor, -rightMotor);
    } else {
        RDTPPacket_updateValue(packet, LeftMotor, 0);
        RDTPPacket_updateValue(packet, RightMotor, 0);
    }
    
    return;
}

- (NSData *)makePacketData
{
    for (ServoLimitter *limitter in limitters) {
        [limitter update];
    }
    RDTPPacketBuffer buf;
    int length;
    RDTPPacket_getSendData(packet, &buf, &length);
    if (length) {
        return [NSData dataWithBytes:buf.buffer length:length];
    } else {
        return nil;
    }
}

@end
