#import <XCTest/XCTest.h>
#import "RDTPPacket.h"

@interface RDTPPacketTests : XCTestCase

@end

@implementation RDTPPacketTests
{
    RDTPPacket packet;
    RDTPPacketBuffer buf;
    int length;
}

- (void)setUp
{
    [super setUp];
    RDTPPacket_init(&packet);
    length = 0;
}

- (void)testSmoke
{
    RDTPPacket_updateValue(&packet, LeftMotor, 42);
    RDTPPacket_updateValue(&packet, RightMotor, 24);
    RDTPPacket_getSendData(&packet, &buf, &length);
    XCTAssertEqual(length, 4);
    XCTAssertEqual(buf.header, (1 << LeftMotor) | (1 << RightMotor));
    XCTAssertEqual(buf.values[0], 42);
    XCTAssertEqual(buf.values[1], 24);
    
    RDTPPacket_updateValue(&packet, Servo1, 0);
    RDTPPacket_getSendData(&packet, &buf, &length);
    XCTAssertEqual(length, 3);
    XCTAssertEqual(buf.header, 1 << Servo1);
    XCTAssertEqual(buf.values[0], 0);
    
    RDTPPacket_updateValue(&packet, LeftMotor, 127);
    RDTPPacket_updateValue(&packet, RightMotor, -42);
    RDTPPacket_getSendData(&packet, &buf, &length);
    XCTAssertEqual(length, 4);
    XCTAssertEqual(buf.header, (1 << LeftMotor) | (1 << RightMotor));
    XCTAssertEqual(buf.values[0], 127);
    XCTAssertEqual(buf.values[1], -42);
    
    RDTPPacket_updateValue(&packet, RightMotor, 5);
    RDTPPacket_updateValue(&packet, RightMotor, 78);
    RDTPPacket_getSendData(&packet, &buf, &length);
    XCTAssertEqual(length, 3);
    XCTAssertEqual(buf.header, 1 << RightMotor);
    XCTAssertEqual(buf.values[0], 78);
    
    RDTPPacket_updateValue(&packet, Servo9, 102);
    RDTPPacket_updateValue(&packet, Servo7, 52);
    RDTPPacket_getSendData(&packet, &buf, &length);
    XCTAssertEqual(length, 4);
    XCTAssertEqual(buf.header, 1 << Servo7 | 1 << Servo9);
    XCTAssertEqual(buf.values[0], 52);
    XCTAssertEqual(buf.values[1], 102);
    
    RDTPPacket_getSendData(&packet, &buf, &length);
    XCTAssertEqual(length, 0);
    
    RDTPPacket_updateValue(&packet, Servo9, 45);
    RDTPPacket_getSendData(&packet, &buf, &length);
    XCTAssertEqual(length, 3);
    XCTAssertEqual(buf.header, 1 << Servo9);
    XCTAssertEqual(buf.values[0], 45);
    
    RDTPPacket_updateValue(&packet, Servo0, 100);
    RDTPPacket_getSendData(&packet, &buf, &length);
    XCTAssertEqual(length, 3);
    XCTAssertEqual(buf.header, 1 << Servo0);
    XCTAssertEqual(buf.values[0], 100);
    
    RDTPPacket_setCommand(&packet, StartVideo);
    RDTPPacket_getSendData(&packet, &buf, &length);
    XCTAssertEqual(length, 3);
    XCTAssertEqual(buf.header, 0);
    XCTAssertEqual(buf.values[0], StartVideo);
}

- (void)testBoundary
{
    RDTPPacket_updateValue(&packet, LeftMotor, INT8_MAX);
    RDTPPacket_updateValue(&packet, RightMotor, INT8_MAX);
    RDTPPacket_updateValue(&packet, Servo0, INT8_MAX);
    RDTPPacket_updateValue(&packet, Servo1, INT8_MAX);
    RDTPPacket_updateValue(&packet, Servo2, INT8_MAX);
    RDTPPacket_updateValue(&packet, Servo3, INT8_MAX);
    RDTPPacket_updateValue(&packet, Servo4, INT8_MAX);
    RDTPPacket_updateValue(&packet, Servo5, INT8_MAX);
    RDTPPacket_updateValue(&packet, Servo6, INT8_MAX);
    RDTPPacket_updateValue(&packet, Servo7, INT8_MAX);
    RDTPPacket_updateValue(&packet, Servo8, INT8_MAX);
    RDTPPacket_updateValue(&packet, Servo9, INT8_MAX);
    RDTPPacket_getSendData(&packet, &buf, &length);
    XCTAssertEqual(length, 14);
    XCTAssertEqual(buf.header,
                   (1 << LeftMotor) | (1 << RightMotor) | (1 << Servo0)
                   | (1 << Servo1) | (1 << Servo2) | (1 << Servo3)
                   | (1 << Servo4) | (1 << Servo5) | (1 << Servo6)
                   | (1 << Servo7) | (1 << Servo8) | (1 << Servo9));
    for (int i = 0; i < sizeof(buf.values) / sizeof(buf.values[0]); ++i) {
        XCTAssertEqual(buf.values[i], INT8_MAX);
    }
    
    RDTPPacket_getSendData(&packet, &buf, &length);
    XCTAssertEqual(length, 0);
    
    RDTPPacket_updateValue(&packet, LeftMotor, INT8_MIN);
    RDTPPacket_updateValue(&packet, RightMotor, INT8_MIN);
    RDTPPacket_updateValue(&packet, Servo0, INT8_MIN);
    RDTPPacket_updateValue(&packet, Servo1, INT8_MIN);
    RDTPPacket_updateValue(&packet, Servo2, INT8_MIN);
    RDTPPacket_updateValue(&packet, Servo3, INT8_MIN);
    RDTPPacket_updateValue(&packet, Servo4, INT8_MIN);
    RDTPPacket_updateValue(&packet, Servo5, INT8_MIN);
    RDTPPacket_updateValue(&packet, Servo6, INT8_MIN);
    RDTPPacket_updateValue(&packet, Servo7, INT8_MIN);
    RDTPPacket_updateValue(&packet, Servo8, INT8_MIN);
    RDTPPacket_updateValue(&packet, Servo9, INT8_MIN);
    RDTPPacket_getSendData(&packet, &buf, &length);
    XCTAssertEqual(length, 14);
    XCTAssertEqual(buf.header,
                   (1 << LeftMotor) | (1 << RightMotor) | (1 << Servo0)
                   | (1 << Servo1) | (1 << Servo2) | (1 << Servo3)
                   | (1 << Servo4) | (1 << Servo5) | (1 << Servo6)
                   | (1 << Servo7) | (1 << Servo8) | (1 << Servo9));
    for (int i = 0; i < sizeof(buf.values) / sizeof(buf.values[0]); ++i) {
        XCTAssertEqual(buf.values[i], INT8_MIN);
    }
}

- (void)testRandom
{
    RDTPPacket receivePacket;
    for (int i = 0; i < 100; ++i) {
        int numOfComponents = arc4random_uniform(Servo3 + 1) + 1;
        NSMutableSet *components = [[NSMutableSet alloc] initWithCapacity:numOfComponents];
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithCapacity:numOfComponents];
        while (components.count != numOfComponents) {
            [components addObject:@(arc4random_uniform(Servo3 + 1))];
        }
        for (NSNumber *component in components) {
            int numOfDummyAssign = arc4random_uniform(10);
            for (int i = 0; i < numOfDummyAssign; ++i) {
                RDTPPacket_updateValue(&packet,
                                       (RDTPPacketComponent)[component intValue],
                                       arc4random_uniform(UINT8_MAX) - INT_MAX);
            }
            int8_t value = arc4random_uniform(UINT8_MAX) - INT_MAX;
            RDTPPacket_updateValue(&packet,
                                   (RDTPPacketComponent)[component intValue],
                                   value);
            [dictionary setObject:@(value) forKey:component];
        }
        RDTPPacket_getSendData(&packet, &buf, &length);
        
        RDTPPacket_initWithBytes(&receivePacket, buf.buffer, length);
        int8_t value;
        RDTPPacketComponent component;
        while (RDTPPacket_getReceiveData(&receivePacket, &value, &component) == DataAvailable) {
            NSNumber *componentObj = @(component);
            NSNumber *dictValue = [dictionary objectForKey:componentObj];
            XCTAssertEqual([dictValue charValue], value);
            [components removeObject:componentObj];
        }
        XCTAssertEqual([components count], 0);
    }
}

@end
