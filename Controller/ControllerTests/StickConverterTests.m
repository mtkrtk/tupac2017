#import <XCTest/XCTest.h>
#import "StickConverter.h"
#import "GamepadController.h"

@interface NSArray (StickConverterTestSorter)

- (NSArray *)sortedArray;

@end

@implementation NSArray (StickConverterTestSorter)

- (NSArray *)sortedArray
{
    return [self sortedArrayUsingComparator:^NSComparisonResult(StickConverterResult *obj1, StickConverterResult *obj2) {
        if (obj1.component > obj2.component) {
            return NSOrderedDescending;
        } else if (obj1.component < obj2.component) {
            return NSOrderedAscending;
        } else {
            return NSOrderedSame;
        }
    }];
}

@end

@interface StickConverterTests : XCTestCase

@end

@implementation StickConverterTests
{
    StickConverter *converter;
    NSArray<StickConverterResult *> *results;
}

- (void)setUp
{
    converter = [[StickConverter alloc] init];
    results = nil;
}

- (void)testSmoke
{
    results = [[converter convertFrom:RightTrigger withValue:GAMEPAD_MAX] sortedArray];
    XCTAssertEqual(results.count, 2);
    XCTAssertEqual(results[0].component, LeftMotor);
    XCTAssertEqual(results[0].value, INT8_MAX);
    XCTAssertEqual(results[1].component, RightMotor);
    XCTAssertEqual(results[1].value, INT8_MAX);
    
    results = [[converter convertFrom:RightTrigger withValue:0] sortedArray];
    XCTAssertEqual(results.count, 2);
    XCTAssertEqual(results[0].component, LeftMotor);
    XCTAssertEqual(results[0].value, INT8_MAX / 2);
    XCTAssertEqual(results[1].component, RightMotor);
    XCTAssertEqual(results[1].value, INT8_MAX / 2);
    
    results = [converter convertFrom:LeftStickY withValue:GAMEPAD_MAX];
    XCTAssertEqual(results.count, 1);
    XCTAssertEqual(results[0].component, Servo1);
    XCTAssertEqual(results[0].value, INT8_MAX);
    
    results = [[converter convertFrom:LeftStickX withValue:GAMEPAD_MAX] sortedArray];
    XCTAssertEqual(results.count, 2);
    XCTAssertEqual(results[0].component, LeftMotor);
    XCTAssertEqual(results[0].value, INT8_MAX / 2);
    XCTAssertEqual(results[1].component, RightMotor);
    XCTAssertEqual(results[1].value, 0);
    
    results = [[converter convertFrom:RightTrigger withValue:GAMEPAD_MIN] sortedArray];
    XCTAssertEqual(results.count, 2);
    XCTAssertEqual(results[0].component, LeftMotor);
    XCTAssertEqual(results[0].value, 0);
    XCTAssertEqual(results[1].component, RightMotor);
    XCTAssertEqual(results[1].value, 0);
    
    results = [[converter convertFrom:LeftTrigger withValue:GAMEPAD_MAX] sortedArray];
    XCTAssertEqual(results.count, 2);
    XCTAssertEqual(results[0].component, LeftMotor);
    XCTAssertEqual(results[0].value, -INT8_MAX);
    XCTAssertEqual(results[1].component, RightMotor);
    XCTAssertEqual(results[1].value, 0);
    
    results = [converter convertFrom:LeftStickY withValue:GAMEPAD_MIN];
    XCTAssertEqual(results.count, 1);
    XCTAssertEqual(results[0].component, Servo1);
    XCTAssertEqual(results[0].value, -INT8_MAX);
    
    results = [[converter convertFrom:LeftStickX withValue:GAMEPAD_MIN / 2] sortedArray];
    XCTAssertEqual(results.count, 2);
    XCTAssertEqual(results[0].component, LeftMotor);
    XCTAssertEqual(results[0].value, -INT8_MAX / 2);
    XCTAssertEqual(results[1].component, RightMotor);
    XCTAssertEqual(results[1].value, -INT8_MAX);
    
    results = [converter convertFrom:LeftStickY withValue:0];
    XCTAssertEqual(results.count, 1);
    XCTAssertEqual(results[0].component, Servo1);
    XCTAssertEqual(results[0].value, 0);
}

@end
