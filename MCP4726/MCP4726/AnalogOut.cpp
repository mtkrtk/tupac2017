#include "AnalogOut.hpp"

mbed::AnalogOut::AnalogOut(PinName sda, PinName scl) : i2c(sda, scl)
{
//    i2c_init(&i2c, sda, scl);
    i2c.frequency(400e3);
    char buf;
    
    buf = 0x06;
    i2c.write(0x00, &buf, 1);
    
    buf = 0x09;
    i2c.write(0x00, &buf, 1);
    
    buf = 0x80;
    i2c.write(address, &buf, 1);
}

//mbed::AnalogOut::AnalogOut(PinName mosi, PinName miso, PinName sclk, PinName ssel) : spi(mosi, miso, sclk, ssel)
//{
//    spi.frequency(400e3);
//}

void mbed::AnalogOut::write_u16(uint16_t value)
{
    value >>= 4;
//    spi.write(0xC0);
//    spi.write(value >> 8);
//    spi.write(value & 0xFF);
    char buf[2];
    buf[0] = 0x0F & (value >> 8);
    buf[1] = value & 0xFF;
    i2c.write(address, buf, 2);
}

void mbed::AnalogOut::write(float value)
{
    uint16_t integerValue = 0x0FFF * value;
    char buf[2];
    buf[0] = 0x0F & (integerValue >> 8);
    buf[1] = integerValue & 0xFF;
//    if (i2c.write(address, buf, 2) == 2) {
//        led = ! led;
//    }
    i2c.write(address, buf, 2);
//    spi.write(0xC0);
//    spi.write(integerValue >> 8);
//    spi.write(integerValue & 0xFF);
}
