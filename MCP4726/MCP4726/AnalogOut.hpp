#ifndef __AnalogOut__
#define __AnalogOut__

#include "mbed.h"

namespace mbed {
    
class AnalogOut {
    static constexpr int address = 0xC0;
    mbed::I2C i2c;
//    mbed::SPI spi;
    
public:
    AnalogOut(PinName sda, PinName scl);
    AnalogOut(PinName mosi, PinName miso, PinName sclk, PinName ssel);
    void write_u16(uint16_t value);
    void write(float value);
    AnalogOut& operator = (float percent) {
        write(percent);
        return *this;
    }
};
    
}

#endif
