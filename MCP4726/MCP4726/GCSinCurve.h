#ifndef __GCSinCurve__
#define __GCSinCurve__

#include <math.h>
#include "mbed_assert.h"

#define DEG_TO_RAD 0.017453292519943295769236907684886
#define radians(deg) ((deg)*DEG_TO_RAD)

static inline constexpr double safeRadians(int degrees)
{
    return degrees > 90 && degrees <= 180 ? radians(180 - degrees) :
    degrees > 180 && degrees <= 270 ? radians(180 - degrees) :
    degrees > 270 ? safeRadians(degrees - 360) :
    degrees < -90 ? safeRadians(degrees + 360) :
    radians(degrees);
}

template <int numOfSteps>
class GCSinCurve {
private:
    static bool isInitialized;
    static float inflatingSinValues[numOfSteps];
    static float deflatingSinValues[numOfSteps];
    static void fillSinValues(float *valuesPoint, int p) {
        MBED_ASSERT(180 % numOfSteps == 0);
        int step = 180 / numOfSteps;
        for (int x = step; x <= 180; x += step) {
            *valuesPoint = sin(safeRadians(x - p));
            valuesPoint++;
        }
    }
    static void initialize() {
        fillSinValues(inflatingSinValues, 90);
        fillSinValues(deflatingSinValues, -90);
    }
    
    float a = 0;
    float b = 0;
    float currentValue = 0;
    float *sinValues = nullptr;
    int count = 0;
    
public:
    GCSinCurve() {
        if (! isInitialized) {
            initialize();
            isInitialized = true;
        }
    }
    void setTargetValue(float value) {
        if (currentValue < value) {
            sinValues = inflatingSinValues;
            a = (value - currentValue) / 2;
        } else {
            sinValues = deflatingSinValues;
            a = (currentValue - value) / 2;
        }
        b = (currentValue + value) / 2;
        count = numOfSteps;
    }
    float next() {
        if (count) {
            count--;
            currentValue = a * (*sinValues++) + b;
        }
        return currentValue;
    }
    bool available() {
        return count;
    }
    void skipToTheEnd() {
        count = 0;
    }
    float readCurrentValue() {
        return currentValue;
    }
};

template <int numOfSteps> bool  GCSinCurve<numOfSteps>::isInitialized = false;
template <int numOfSteps> float GCSinCurve<numOfSteps>::inflatingSinValues[];
template <int numOfSteps> float GCSinCurve<numOfSteps>::deflatingSinValues[];

#endif
