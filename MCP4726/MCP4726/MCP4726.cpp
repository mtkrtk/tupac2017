#include "MCP4726.hpp"

MCP4726::MCP4726(PinName sda, PinName scl) : i2c(sda, scl)
{
    i2c.frequency(400e3);
    char buf;
    
    buf = 0x06;
    i2c.write(0x00, &buf, 1);
    
    buf = 0x09;
    i2c.write(0x00, &buf, 1);
    
    buf = 0x80;
    i2c.write(address, &buf, 1);
}

static void i2cCallback(int status)
{
    
}

void MCP4726::write(char *raw)
{
//    i2c.write(address, raw, 2);
    i2c.transfer(address, raw, 2, nullptr, 0, i2cCallback);
}
