#ifndef __MCP4726__
#define __MCP4726__

#include "mbed.h"

class MCP4726 {
    static constexpr int address = 0xC0;
    mbed::I2C i2c;
    
public:
    MCP4726(PinName sda, PinName scl);
    void write(char *raw);
};

#endif
