#include "WaveFile.hpp"
#include "mbed.h"

typedef struct {
    int16_t  comp_code;
    int16_t  num_channels;
    uint32_t sample_rate;
    uint32_t avg_Bps;
    int16_t  block_align;
    int16_t  sig_bps;
} FMT_STRUCT;

WaveFile::WaveFile(FILE *_wav) : wav(_wav)
{
    printf("init\n");
    fflush(stdout);
    for (int i = 0; i < bufSize - 1; ++i) {
        buffer[i].next = &buffer[i + 1];
    }
    buffer[bufSize - 1].next = &buffer[0];
    
    int chunk_id, chunk_size, data, samp_int;
    long num_slices;
    FMT_STRUCT wav_format;
    
    fread(&chunk_id, 4, 1, wav);
    fread(&chunk_size, 4, 1, wav);
    printf("Read chunk ID 0x%X, size 0x%X\n", chunk_id, chunk_size);
    
    while (! feof(wav)) {
        switch (chunk_id) {
            case 0x46464952:
                fread(&data, 4, 1, wav);
                printf("RIFF chunk\n");
                printf("  chunk size %d (0x%X)\n", chunk_size, chunk_size);
                printf("  RIFF type 0x%X\n", data);
                break;
                
            case 0x20746d66:
                fread(&wav_format, sizeof(wav_format), 1, wav);
                printf("FORMAT chunk\n");
                printf("  chunk size %d (0x%X)\n", chunk_size, chunk_size);
                printf("  compression code %d\n", wav_format.comp_code);
                printf("  %d channels\n", wav_format.num_channels);
                printf("  %lu samples/sec\n", wav_format.sample_rate);
                printf("  %lu bytes/sec\n", wav_format.avg_Bps);
                printf("  block align %d\n", wav_format.block_align);
                printf("  %d bits per sample\n", wav_format.sig_bps);
                if (chunk_size > static_cast<int>(sizeof(wav_format)))
                    fseek(wav, chunk_size - sizeof(wav_format), SEEK_CUR);
                break;
                
            case 0x61746164:
                num_slices = chunk_size / wav_format.block_align;
                samp_int = 1000000 / (wav_format.sample_rate);
                printf("DATA chunk\n");
                printf("  chunk size %d (0x%X)\n", chunk_size,chunk_size);
                printf("  %ld slices\n", num_slices);
                printf("  Ideal sample interval=%d\n", (unsigned)(1000000.0 / wav_format.sample_rate));
                printf("  programmed interrupt tick interval=%d\n", samp_int);
                samplingInterval = samp_int;
                
                for (int i = 0; i < bufSize; ++i) {
                    union {
                        uint16_t value;
                        uint8_t arr[2];
                    } buf;
                    fread(&buf.value, sizeof(buf.value), 1, wav);
                    buf.value = (static_cast<int>(buf.value) + 32768) >> 4;
                    currentWriteNode->value[0] = 0x0F & buf.value >> 8;
                    currentWriteNode->value[1] = 0xFF & buf.value;
                    currentWriteNode = currentWriteNode->next;
                    if (feof(wav)) {
                        break;
                    }
                }
                
                return;
                
            case 0x5453494c:
                printf("INFO chunk, size %d\n", chunk_size);
                fseek(wav, chunk_size, SEEK_CUR);
                break;
                
            default:
                printf("unknown chunk type 0x%X, size %d\n", chunk_id, chunk_size);
                data=fseek(wav, chunk_size, SEEK_CUR);
                break;
        }
        
        fread(&chunk_id, 4, 1, wav);
        fread(&chunk_size, 4, 1, wav);
    }
}

void WaveFile::dispatchRead()
{
    while (1) {
        union {
            uint16_t value;
            uint8_t arr[2];
        } buf;
        fread(&buf.value, sizeof(buf.value), 1, wav);
        while (currentWriteNode == currentReadNode) {
            __DSB();
        }
        buf.value = (static_cast<int>(buf.value) + 32768) >> 4;
        currentWriteNode->value[0] = 0x0F & buf.value >> 8;
        currentWriteNode->value[1] = buf.value;
        currentWriteNode = currentWriteNode->next;
    }
}

#include "rtos.h"
static MCP4726 *globalDAC;
static rtos::Semaphore sema;

void WaveFile::writeToDAC()
{
    while (1) {
        sema.wait();
        globalDAC->write((currentReadNode->value));
        currentReadNode = currentReadNode->next;
    }
}

static void signaler()
{
    sema.release();
}

void WaveFile::play(MCP4726 *dac)
{
    globalDAC = dac;
    unsigned char stack[DEFAULT_STACK_SIZE];
    rtos::Thread readingThread(osPriorityBelowNormal, DEFAULT_STACK_SIZE, stack);
    readingThread.start(mbed::callback(this, &WaveFile::dispatchRead));
    mbed::Ticker ticker;
    ticker.attach_us(signaler, samplingIntervalInUs());
    writeToDAC();
    
//    mbed::Ticker ticker;
//    ticker.attach_us(mbed::callback(this, &WaveFile::writeToDAC), samplingIntervalInUs());
//    while (1) {
//        union {
//            uint16_t value;
//            uint8_t arr[2];
//        } buf;
//        fread(&buf.value, sizeof(buf.value), 1, wav);
//        while (currentWriteNode == currentReadNode) {
//            __DSB();
//        }
//        buf.value = (static_cast<int>(buf.value) + 32768) >> 4;
//        currentWriteNode->value[0] = 0x0F & buf.value >> 8;
//        currentWriteNode->value[1] = 0xFF & buf.value;
//        currentWriteNode = currentWriteNode->next;
//    }
}
