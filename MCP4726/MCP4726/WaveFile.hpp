#ifndef __WaveFile__
#define __WaveFile__

#include "mbed.h"
#include "AnalogOut.hpp"
#include "MCP4726.hpp"

class WaveFile {
    static constexpr int bufSize = 1024;
    FILE *wav;
    struct DataNode {
        char value[2];
        DataNode *next;
    };
    DataNode buffer[bufSize];
    DataNode *currentReadNode = buffer;
    DataNode *currentWriteNode = buffer;
    int samplingInterval = 0;
    void writeToDAC();
    
public:
    WaveFile(FILE *wav);
    bool next(uint16_t *value) {
        if (currentReadNode) {
//            *value = currentReadNode->value;
            currentReadNode = currentReadNode->next;
            return true;
        } else {
            return false;
        }
    }
    int samplingIntervalInUs() {
        return samplingInterval;
    }
    void dispatchRead();
    void play(MCP4726 *dac);
};

#endif
