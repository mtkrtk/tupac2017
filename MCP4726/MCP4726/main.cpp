#include "mbed.h"
//#include "rtos.h"
#include "AnalogOut.hpp"
#include "GCSinCurve.h"
#include "SDFileSystem.h"
#include "WaveFile.hpp"
#include "MCP4726.hpp"
#include "wave_player.h"

static WaveFile *globalWav;
static mbed::AnalogOut *globalDac;

void readThread()
{
    globalWav->dispatchRead();
}

void writeToDac()
{
    uint16_t value;
    globalWav->next(&value);
    globalDac->write_u16(value);
}

int main(int MBED_UNUSED argc, const char MBED_UNUSED * argv[])
{
    MCP4726 dac(P1_7, P1_6);
//    globalDac = &dac;
    mbed::Serial pc(USBTX, USBRX);
    pc.baud(115200);
//    mbed::AnalogOut dac(P10_14, P10_15, P10_12, P10_13);
//    const double freq = 50;
//    constexpr int numOfSplitsPerHalfPeriod = 10;
//    const int intervalInUs = 1e6 / freq / numOfSplitsPerHalfPeriod / 2;
//    GCSinCurve<numOfSplitsPerHalfPeriod> curve;
//    mbed::Timer timer;
//    timer.start();
//    while (1) {
//        curve.setTargetValue(1.0);
//        do {
//            timer.reset();
//            dac = curve.next();
//            while (timer.read_us() < intervalInUs);
//        } while (curve.available());
//        curve.setTargetValue(0);
//        do {
//            timer.reset();
//            dac = curve.next();
//            while (timer.read_us() < intervalInUs);
//        } while (curve.available());
//    }
    
    SDFileSystem sd(P8_5, P8_6, P8_3, P8_4, "sd");
    FILE *wav = fopen("/sd/harebare.wav", "r");
    WaveFile waveFile(wav);
    globalWav = &waveFile;
    
//    unsigned char stack[DEFAULT_STACK_SIZE];
//    rtos::Thread readingThread(osPriorityNormal, DEFAULT_STACK_SIZE, stack);
//    readingThread.start(readThread);
//    mbed::Timer timer;
//    int interval = waveFile.samplingIntervalInUs();
//    union {
//        uint16_t value;
//        char array[2];
//    } buf;
//    timer.start();
//    while (waveFile.next(&buf.value)) {
////        int16_t raw = buf.array[0] << 8 | buf.array[1];
//        while (timer.read_us() < interval) ;
////        dac.write(buf.value);
//        dac.write_u16(buf.value);
//        pc.putc('o');
//        timer.reset();
//    }
    
//    mbed::Ticker ticker;
//    ticker.attach_us(writeToDac, waveFile.samplingIntervalInUs());
//    waveFile.dispatchRead();
//    while (1) {
//        rtos::Thread::yield();
//    }
    waveFile.play(&dac);
    
    fclose(wav);
    
    return 0;
}
