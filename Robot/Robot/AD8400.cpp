#include "AD8400.hpp"

AD8400::AD8400(PinName mosi, PinName scl, PinName _ss) : spi(mosi, NC, scl), ss(_ss), callback(this, &AD8400::spiCallback)
{
    ss = 1;
    buf[0] = 0;
    spi.frequency(25000000);
}

void AD8400::spiCallback(int)
{
    ss = 1;
}

AD8400& AD8400::operator = (uint8_t value)
{
    ss = 0;
    buf[1] = value;
    spi.transfer(buf, 2, static_cast<char *>(nullptr), 0, callback, SPI_EVENT_ALL);
    return *this;
}
