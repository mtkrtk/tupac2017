#ifndef __AD8400__
#define __AD8400__

#include "mbed.h"

class AD8400 {
    mbed::SPI spi;
    mbed::DigitalOut ss;
    char buf[2];
    void spiCallback(int status);
    mbed::event_callback_t callback;
    
public:
    AD8400(PinName mosi, PinName scl, PinName ss);
    AD8400& operator = (uint8_t value);
};

#endif
