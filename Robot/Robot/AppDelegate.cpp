#include "AppDelegate.hpp"
#include <limits>
#include "dec_wav.h"

static constexpr int audioBufNum = 20;
static constexpr int audioBufSize = 0x1B90;
static char __attribute__((section("NC_BSS"), aligned(4))) audioBuf[audioBufNum][audioBufSize];

static void callback_audio_write_end(void *, int32_t result, void *)
{
    if (result < 0) {
        printf("audio write callback error %ld\n", result);
    }
}

AppDelegate::AppDelegate(RDTPv2 *app) : application(app), volume(P10_14, P10_12, P10_13), sd(P8_5, P8_6, P8_3, P8_4, "sd"), audio(P2_4, P2_5, P2_7, P2_6, 0x80, audioBufNum - 1, 0), audio_write_async_ctl({&callback_audio_write_end, NULL})
{
    application->delegate = this;
    video->initWithDelegate(this);
    for (int i = 0; i < Servo9 - Servo0 + 1; ++i) {
        if (servoStatus[i]) {
            servos[i] = 0.5;
        } else {
            servos[i].relux();
        }
    }
    volume = 128;
    
    ssif_channel_cfg_t ssif_cfg;
    ssif_cfg.enabled                = true;
    ssif_cfg.int_level              = 0x78;
    ssif_cfg.slave_mode             = false;
    ssif_cfg.sample_freq            = 44100;
    ssif_cfg.clk_select             = SSIF_CFG_CKS_AUDIO_X1;
    ssif_cfg.multi_ch               = SSIF_CFG_MULTI_CH_1;
    ssif_cfg.data_word              = SSIF_CFG_DATA_WORD_16;
    ssif_cfg.system_word            = SSIF_CFG_SYSTEM_WORD_16;
    ssif_cfg.bclk_pol               = SSIF_CFG_FALLING;
    ssif_cfg.ws_pol                 = SSIF_CFG_WS_HIGH;
    ssif_cfg.padding_pol            = SSIF_CFG_PADDING_LOW;
    ssif_cfg.serial_alignment       = SSIF_CFG_DATA_FIRST;
    ssif_cfg.parallel_alignment     = SSIF_CFG_LEFT;
    ssif_cfg.ws_delay               = SSIF_CFG_NO_DELAY;
    ssif_cfg.noise_cancel           = SSIF_CFG_ENABLE_NOISE_CANCEL;
    ssif_cfg.tdm_mode               = SSIF_CFG_DISABLE_TDM;
    ssif_cfg.romdec_direct.mode     = SSIF_CFG_DISABLE_ROMDEC_DIRECT;
    ssif_cfg.romdec_direct.p_cbfunc = NULL;
    audio.ConfigChannel(&ssif_cfg);
}

void AppDelegate::application_didReceiveCommand(RDTP MBED_UNUSED *app, RDTPPacket *packet, Transport::Header &&source)
{
    RDTPPacketComponent component;
    int8_t value;
    while (1) {
        switch (RDTPPacket_getReceiveData(packet, &value, &component)) {
            case DataAvailable:
                switch (component) {
                    case LeftMotor:
//                        printf("L%d\n", value);
                        leftMotor.forward(value);
                        break;
                        
                    case RightMotor:
//                        printf("R%d\n", value);
                        rightMotor.forward(value);
                        break;
                        
                    case Servo0:
                    case Servo1:
                    case Servo2:
                    case Servo3:
                    case Servo4:
                    case Servo5:
                    case Servo6:
                    case Servo7:
                    case Servo8:
                    case Servo9:
//                        if (value) {
//                            servos[component - Servo0] = (value + maxMoterPower) / static_cast<float>(maxServoPower);
//                        } else {
//                            servos[component - Servo0].relux();
//                        }
                        if (servoStatus[component - Servo0]) {
                            servos[component - Servo0] = (value + maxMoterPower) / static_cast<float>(maxServoPower);
                        }
                        break;
                        
                    case EnableServo:
                        servoStatus[value] = true;
                        break;
                        
                    case DisableServo:
                        servoStatus[value] = false;
                        servos[value].relux();
                        break;
                        
                    default:
                        break;
                }
                break;
                
            case EndOfPacket:
                return;
                
            case CommandAvailable:
                switch (RDTPPacket_getReceiveCommand(packet)) {
                    case StartVideo:
                        videoDestination = source;
                        video->start();
                        break;
                        
                    case StopVideo:
                        video->stop();
                        break;
                        
                    case SayHello:
                    {
                        FATFileHandle file((FIL()));
                        if (sd.open("hello.wav", O_RDONLY, &file)) {
                            dec_wav wav;
                            wav.AnalyzeHeder(nullptr, nullptr, nullptr, 0, &file);
                            size_t size = 1;
                            int bufIndex = 0;
                            while (size) {
                                size = wav.GetNextData(audioBuf[bufIndex], audioBufSize);
                                audio.write(audioBuf[bufIndex], size, &audio_write_async_ctl);
                                ++bufIndex;
                                if (bufIndex == audioBufNum) {
                                    bufIndex = 0;
                                }
                            }
                            file.closeWithoutDealloc();
                        }
                        break;
                    }
                        
                    default:
                        break;
                }
                return;
                
            default:
                return;
        }
    }
}

void AppDelegate::videoConverter_imageAvailable(VideoConverter MBED_UNUSED *sender, uint8_t *jpeg, size_t length)
{
    application->sendDataTo(videoDestination, jpeg, length);
}
