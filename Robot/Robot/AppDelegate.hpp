#ifndef __AppDelegate__
#define __AppDelegate__

#include "RDTPv2.hpp"
#include "mbed.h"
#include "GCMotor.h"
#include "SBRS5314.hpp"
#include "VideoConverter.hpp"
#include <limits>
#include "AD8400.hpp"
#include "SDFileSystem.h"
#include "R_BSP_Ssif.h"

class AppDelegate : public RDTP::Delegate, public VideoConverter::Delegate {
    static constexpr int8_t maxMoterPower  = std::numeric_limits<int8_t>::max();
    static constexpr uint8_t maxServoPower = std::numeric_limits<uint8_t>::max();
    GCMotor rightMotor = GCMotor(P2_3, P2_6, P8_11);
    GCMotor leftMotor = GCMotor(P2_2, P2_0, P8_13);
    SBRS5314 servos[10] = {
        P4_0, P5_3,  P5_0, P4_4, P3_8,
        P5_5, P8_14, P4_6, P2_1, P3_10
    };
    VideoConverter *video = VideoConverter::singleton();
    Transport::Header videoDestination;
    RDTPv2 *application;
    bool servoStatus[10] = {
        true, true, true, true, false,
        true, true, true, true, false
    };
    AD8400 volume;
    SDFileSystem sd;
    R_BSP_Ssif audio;
    rbsp_data_conf_t audio_write_async_ctl;
    
    void application_didReceiveCommand(RDTP *app, RDTPPacket *packet, Transport::Header &&source);
    void videoConverter_imageAvailable(VideoConverter *sender, uint8_t *jpeg, size_t length);
    
public:
    AppDelegate() = delete;
    AppDelegate(RDTPv2 *app);
};

#endif
