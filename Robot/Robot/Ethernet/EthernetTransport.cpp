#include "EthernetTransport.hpp"
#include "EthernetUtil.hpp"
#include <cstring>

EthernetTransport::EthernetTransport()
{
    interface.init();
    interface.connect();
    socket.init();
    socket.bind(port);
    
    printf("Listening on %s:%d\n", interface.getIPAddress(), port);
}

void EthernetTransport::init()
{
    socket.set_broadcasting(true);
    delegate->transport_interfaceReady(this);
    socket.set_broadcasting(false);
}

Transport::Header EthernetTransport::readMyAddress()
{
    Transport::Header header = {};
    char *address = interface.getIPAddress();
    char buf[4];
    int startIndex = 0;
    int endIndex = 0;
    for (int i = 0; i < 4; i++) {
        for (; address[endIndex] != '.'; endIndex++) ;
        memcpy(buf, address + startIndex, endIndex - startIndex);
        buf[endIndex - startIndex] = '\0';
        header.address_arr[i] = atoi(buf);
    }
    return header;
}

void EthernetTransport::sendPacket(Transport::Packet packet)
{
    Endpoint dest;
    char host[16];
    uint8_t *address = packet.destination.address_arr;
    sprintf(host, "%d.%d.%d.%d", address[0], address[1], address[2], address[3]);
    dest.set_address(host, packet.destination.port);
    socket.sendTo(dest, reinterpret_cast<char *>(packet.content), packet.contentLength);
}

void EthernetTransport::run()
{
    char buf[64];
    Endpoint source;
    Packet packet;
    packet.content = reinterpret_cast<uint8_t *>(buf);
    while (1) {
        int length = socket.receiveFrom(source, buf, sizeof(buf));
        if (length > 0) {
            packet.source = EthernetUtil::endPointToHeader(source);
            packet.contentLength = length;
            delegate->transport_didReceivePacket(this, packet);
        }
    }
}
