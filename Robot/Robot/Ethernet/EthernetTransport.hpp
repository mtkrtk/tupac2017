#ifndef __EthernetTransport__
#define __EthernetTransport__

#include "Transport.hpp"
#include "EthernetInterface.h"
#include "Thread.h"
#include "RDTPPacket.h"

class EthernetTransport : public Transport {
    static constexpr uint16_t port = RDTP_PORT;
    EthernetInterface interface;
    UDPSocket socket;
    
public:
    EthernetTransport();
    void init();
    Header readMyAddress();
    void sendPacket(Packet packet);
    void MBED_NORETURN run();
};

#endif
