#include "EthernetUtil.hpp"
#include <cstring>
#include <cstdlib>
#include <cstdio>

namespace EthernetUtil {
    
Transport::Header endPointToHeader(Endpoint endpoint)
{
    Transport::Header header = {};
    char *address = endpoint.get_address();
    char buf[4];
    int startIndex = 0;
    int endIndex = 0;
    for (int i = 0; i < 4; i++) {
        for (; address[endIndex] != '.' && address[endIndex] != '\0'; endIndex++) ;
        memcpy(buf, address + startIndex, endIndex - startIndex);
        buf[endIndex - startIndex] = '\0';
        header.address_arr[i] = atoi(buf);
        ++endIndex;
        startIndex = endIndex;
    }
    header.port = endpoint.get_port();
    return header;
}
    
Endpoint headerToEndPoint(Transport::Header header)
{
    Endpoint endpoint;
    char host[16];
    uint8_t *address = header.address_arr;
    sprintf(host, "%d.%d.%d.%d", address[0], address[1], address[2], address[3]);
    endpoint.set_address(host, header.port);
    return endpoint;
}
    
}
