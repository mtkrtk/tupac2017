#ifndef __EthernetUtil__
#define __EthernetUtil__

#include "Transport.hpp"
#ifdef __MBED__
#include "Endpoint.h"
#else
#include "../../RobotTest/Endpoint.h"
#endif

namespace EthernetUtil {
    Transport::Header endPointToHeader(Endpoint endpoint);
    Endpoint headerToEndPoint(Transport::Header header);
}

#endif
