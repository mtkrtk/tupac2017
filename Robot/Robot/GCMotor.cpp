#include "GCMotor.h"
#include <math.h>
#include <limits>

GCMotor::GCMotor(PinName dig1, PinName dig2, PinName ana)
{
    gpio_init_out(&digital1, dig1);
    gpio_init_out(&digital2, dig2);
    pwmout_init(&analog, ana);
    pwmout_period_us(&analog, 10);
    free();
}

void GCMotor::forward(int8_t power)
{
    if (power > 0) {
        gpio_write(&digital1, 0);
        gpio_write(&digital2, 1);
        pwmout_write(&analog, static_cast<float>(power) / std::numeric_limits<int8_t>::max());
    } else if (power < 0) {
        gpio_write(&digital1, 1);
        gpio_write(&digital2, 0);
        pwmout_write(&analog, -static_cast<float>(power) / std::numeric_limits<int8_t>::max());
    } else {
        free();
    }    
}

void GCMotor::hold()
{
    gpio_write(&digital1, 1);
    gpio_write(&digital2, 1);
    pwmout_write(&analog, 0);
}

void GCMotor::free()
{
//    gpio_write(&digital1, 0);
//    gpio_write(&digital2, 0);
    pwmout_write(&analog, 0);
}
