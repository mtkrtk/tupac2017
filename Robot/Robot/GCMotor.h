#ifndef __GCMotor__
#define __GCMotor__

#include "mbed.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"

class GCMotor {
    gpio_t   digital1;
    gpio_t   digital2;
    pwmout_t analog;
    
public:
    GCMotor(PinName dig1, PinName dig2, PinName ana);
    void forward(int8_t power);
    void hold(void);
    void free(void);
};
#pragma GCC diagnostic pop

#endif
