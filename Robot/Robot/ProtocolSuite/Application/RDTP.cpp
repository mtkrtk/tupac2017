#include "RDTP.hpp"
#include <cstring>
#include <cstdio>

decltype(RDTP::destinationAddress) RDTP::destinationAddress = {255, 255, 255, 255}; /* Broadcast */

RDTP::RDTP(Transport *aTransport) : transport(aTransport)
{
    transport->delegate = this;
    transport->init();
}

void RDTP::greet()
{
    char msg[] = "This is a robot\n";
    Transport::Packet packet = {};
    memcpy(packet.destination.address_arr, destinationAddress, sizeof(destinationAddress));
    packet.destination.port = destinationPort;
    packet.protocol = Transport::Packet::Protocol::UDP;
    packet.content = reinterpret_cast<uint8_t *>(msg);
    packet.contentLength = static_cast<int>(strlen(msg));
    
    transport->sendPacket(packet);
}

void RDTP::transport_interfaceReady(Transport __attribute__((unused)) *aTransport)
{
    greet();
}
