#ifndef __Application__
#define __Application__

#include "Transport.hpp"
#include "RDTPPacket.h"

/* Robot Data Transfer Protocol */
class RDTP : public Transport::Delegate {
protected:
    static const     uint8_t  destinationAddress[4];
    static constexpr uint16_t destinationPort = RDTP_PORT;
    Transport *transport;
    void transport_interfaceReady(Transport *transport);
    
public:
    class Command {
    public:
        typedef int16_t StickValue;
        
        StickValue leftXValue  = 0;
        StickValue leftYValue  = 0;
        StickValue rightXValue = 0;
        StickValue rightYValue = 0;
    };
    
    class Delegate {
    public:
        virtual void application_didReceiveCommand(RDTP *app, RDTPPacket *packet, Transport::Header &&source) = 0;
    };
    
    Delegate *delegate;
    RDTP() = delete;
    RDTP(Transport *transport);
    void greet();
};

#endif
