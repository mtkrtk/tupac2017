#include "RDTPv2.hpp"
#include "mbed_toolchain.h"
#include <utility>

RDTPv2::RDTPv2(Transport *_transport) : RDTP(_transport)
{
    
}

void RDTPv2::transport_didReceivePacket(Transport MBED_UNUSED *transport, Transport::Packet packet)
{
    RDTPPacket rdtp;
    RDTPPacket_initWithBytes(&rdtp, reinterpret_cast<int8_t *>(packet.content), packet.contentLength);
    delegate->application_didReceiveCommand(this, &rdtp, std::move(packet.source));
}

void RDTPv2::sendDataTo(Transport::Header &destination, uint8_t *data, int length)
{
    Transport::Packet packet;
    packet.content = data;
    packet.contentLength = length;
    packet.destination = destination;
    packet.protocol = Transport::Packet::Protocol::UDP;
    transport->sendPacket(packet);
}
