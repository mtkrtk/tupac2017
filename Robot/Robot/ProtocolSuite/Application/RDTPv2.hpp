#ifndef __RDTPv2__
#define __RDTPv2__

#include "RDTP.hpp"

/* Robot Data Transfer Protocol ver. 2 */
class RDTPv2 : public RDTP {
    enum FrameIndex {
        StickDirection,
        LeftStickX,
        LeftStickY,
        RightStickX,
        RightStickY,
    };
    
    enum DirectionBits {
        LeftXPlus,
        LeftYPlus,
        RightXPlus,
        RightYPlus,
    };
    
    void transport_didReceivePacket(Transport *transport, Transport::Packet packet);
    
public:
    RDTPv2(Transport *transport);
    void sendDataTo(Transport::Header &destination, uint8_t *data, int length);
};

#endif
