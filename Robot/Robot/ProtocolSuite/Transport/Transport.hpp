#ifndef __Transport__
#define __Transport__

#include <cstdint>

class Transport {
    
public:
    struct Header {
        union {
            uint32_t address;
            uint8_t  address_arr[4];
        };
        uint16_t port;
    };
    
    struct Packet {
        enum class Protocol {
            TCP,
            UDP,
        };
        
        Header   source;
        Header   destination;
        Protocol protocol;
        uint8_t  *content;
        int      contentLength;
    };
    
    class Delegate {
    public:
        virtual void transport_interfaceReady(Transport *transport) = 0;
        virtual void transport_didReceivePacket(Transport *transport, Packet packet) = 0;
    };
    
    Delegate *delegate;
    virtual void init() = 0;
    virtual Header readMyAddress() = 0;
    virtual void sendPacket(Packet packet) = 0;
};

#endif
