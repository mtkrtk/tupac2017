#include "SBRS5314.hpp"

SBRS5314::SBRS5314(PinName pin)
{
    if (pin == P5_0 || pin == P4_4 || pin == P4_6) {
        gpio_init_out(&gpio, pin);
        gpio_write(&gpio, 0);
        shouldUseTicker = true;
    } else {
        pwmout_init(&pwm, pin);
        pwmout_period_ms(&pwm, 20);
        pwmout_write(&pwm, 0);
    }
}

void SBRS5314::tickerCallback()
{
    if (gpio_read(&gpio)) {
        gpio_write(&gpio, 0);
        ticker.attach_us(mbed::Callback<void ()>(this, &SBRS5314::tickerCallback), 20 * 1000 - currentPulseWidth);
    } else {
        gpio_write(&gpio, 1);
        ticker.attach_us(mbed::Callback<void ()>(this, &SBRS5314::tickerCallback), currentPulseWidth);
    }
}

SBRS5314& SBRS5314::operator=(float percentage)
{
    bool shouldStartTimer = false;
    if (shouldUseTicker && currentPulseWidth == 0) {
        shouldStartTimer = true;
    }
    int nextPulseWidth = dydx * percentage + lowerPulseWidthLimit;
    if (nextPulseWidth < lowerPulseWidthLimit) {
        nextPulseWidth = lowerPulseWidthLimit;
    } else if (nextPulseWidth > upperPulseWidthLimit) {
        nextPulseWidth = upperPulseWidthLimit;
    }
    currentPulseWidth = nextPulseWidth;
    if (shouldUseTicker) {
        if (currentPulseWidth) {
            if (shouldStartTimer) {
                gpio_write(&gpio, 1);
                ticker.attach_us(mbed::Callback<void ()>(this, &SBRS5314::tickerCallback), currentPulseWidth);
            }
        } else {
            gpio_write(&gpio, 0);
            ticker.detach();
        }
    } else {
        pwmout_pulsewidth_us(&pwm, currentPulseWidth);
    }
    
    return *this;
}

void SBRS5314::relux()
{
    if (shouldUseTicker) {
        ticker.detach();
        gpio_write(&gpio, 0);
    } else {
        pwmout_pulsewidth_us(&pwm, 50);
    }
}
