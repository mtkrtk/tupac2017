#ifndef __SBRS5314__
#define __SBRS5314__

#include "mbed.h"

class SBRS5314 {
    static constexpr int upperPulseWidthLimit = 2100;
    static constexpr int lowerPulseWidthLimit = 900;
    static constexpr int dydx = upperPulseWidthLimit - lowerPulseWidthLimit;
    pwmout_t pwm;
    gpio_t gpio;
    mbed::Ticker ticker;
    int currentPulseWidth = 0;
    bool shouldUseTicker = false;
    void tickerCallback();
    
public:
    SBRS5314(PinName pin);
    SBRS5314& operator = (float percentage);
    operator float() {
        return (currentPulseWidth - lowerPulseWidthLimit) / static_cast<float>(dydx);
    }
    void relux();
};

#endif
