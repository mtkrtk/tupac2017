#include "VideoConverter.hpp"
#include <cstdio>
#include "mbed_toolchain.h"
#include "mbed_assert.h"

VideoConverter VideoConverter::singletonObj;
decltype(VideoConverter::FrameBuffer_Video) VideoConverter::FrameBuffer_Video;
decltype(VideoConverter::JpegBuffer) VideoConverter::JpegBuffer;

void VideoConverter::vsyncHandlerStatic(DisplayBase::int_type_t int_type)
{
    singletonObj.vsyncHandler(int_type);
}

void VideoConverter::vfieldHandlerStatic(DisplayBase::int_type_t int_type)
{
    singletonObj.vfieldHandler(int_type);
}

void VideoConverter::jcuHandlerStatic(JPEG_Converter::jpeg_conv_error_t err_code)
{
    singletonObj.jcuHandler(err_code);
}

void VideoConverter::vsyncHandler(DisplayBase::int_type_t MBED_UNUSED int_type)
{
    if (vsyncCount > 0) {
        --vsyncCount;
    }
}

void VideoConverter::vfieldHandler(DisplayBase::int_type_t MBED_UNUSED int_type)
{
    if (vfieldCount > 0) {
        --vfieldCount;
        if (vfieldCount != 0) {
            return;
        } else {
            vfieldCount = 4;
        }
    } else {
        return;
    }

    jcu_encoding = 1;
    if (jcu_buf_index_read == jcu_buf_index_write) {
        if (jcu_buf_index_write != 0) {
            jcu_buf_index_write = 0;
        } else {
            jcu_buf_index_write = 1;
        }
    }
    jcu_encode_size[jcu_buf_index_write] = 0;
    JPEG_Converter::jpeg_conv_error_t error;
    error = Jcu.encode(&bitmap_buff_info, JpegBuffer[jcu_buf_index_write],
                       &jcu_encode_size[jcu_buf_index_write], &encode_options);
    if (error != JPEG_Converter::JPEG_CONV_OK) {
        jcu_encode_size[jcu_buf_index_write] = 0;
        jcu_encoding = 0;
    }
}

void VideoConverter::jcuHandler(JPEG_Converter::jpeg_conv_error_t MBED_UNUSED err_code)
{
    jcu_buf_index_write_done = jcu_buf_index_write;
    image_change = 1;
    jcu_encoding = 0;
}

VideoConverter::VideoConverter()
{
}

VideoConverter *VideoConverter::singleton()
{
    return &singletonObj;
}

void VideoConverter::initWithDelegate(VideoConverter::Delegate *aDelegate)
{
    DisplayBase::graphics_error_t error;
    
    MBED_ASSERT(aDelegate);
    delegate = aDelegate;
    
    bitmap_buff_info.width  = width;
    bitmap_buff_info.height = height;
    bitmap_buff_info.format = JPEG_Converter::WR_RD_YCbCr422;
    bitmap_buff_info.buffer_address = reinterpret_cast<void *>(FrameBuffer_Video);
    
    encode_options.encode_buff_size = sizeof(JpegBuffer[0]);
    encode_options.p_EncodeCallBackFunc = &VideoConverter::jcuHandlerStatic;
    
    /* Graphics initialization process */
    error = display.Graphics_init(NULL);
    if (error != DisplayBase::GRAPHICS_OK) {
        printf("Line %d, error %d\n", __LINE__, error);
        while (1);
    }
    
    error = display.Graphics_Video_init( DisplayBase::INPUT_SEL_VDEC, NULL);
    if( error != DisplayBase::GRAPHICS_OK ) {
        printf("Line %d, error %d\n", __LINE__, error);
        while(1);
    }
    
    /* Interrupt callback function setting (Vsync signal input to scaler 0) */
    error = display.Graphics_Irq_Handler_Set(DisplayBase::INT_TYPE_S0_VI_VSYNC, 0, &VideoConverter::vsyncHandlerStatic);
    if (error != DisplayBase::GRAPHICS_OK) {
        printf("Line %d, error %d\n", __LINE__, error);
        while (1);
    }
    
    /* Video capture setting (progressive form fixed) */
    error = display.Video_Write_Setting(DisplayBase::VIDEO_INPUT_CHANNEL_0, DisplayBase::COL_SYS_PAL_443,
                                        FrameBuffer_Video, VIDEO_BUFFER_STRIDE,
                                        DisplayBase::VIDEO_FORMAT_YCBCR422, DisplayBase::WR_RD_WRSWA_NON,
                                        height, width);
    if (error != DisplayBase::GRAPHICS_OK) {
        printf("Line %d, error %d\n", __LINE__, error);
        while (1);
    }
    
    /* Interrupt callback function setting (Field end signal for recording function in scaler 0) */
    error = display.Graphics_Irq_Handler_Set(DisplayBase::INT_TYPE_S0_VFIELD, 0, &VideoConverter::vfieldHandlerStatic);
    if (error != DisplayBase::GRAPHICS_OK) {
        printf("Line %d, error %d\n", __LINE__, error);
        while (1);
    }
    
    thread.start(mbed::Callback<void ()>(this, &VideoConverter::readAndNotify));
    
    start();
    stop();
    /* Wait vsync to update resister */
    waitVsync(1);
}

void VideoConverter::start()
{
    DisplayBase::graphics_error_t error;
    MBED_ASSERT(delegate);
    
//    thread.start(mbed::Callback<void ()>(this, &VideoConverter::readAndNotify));
    
    /* Video write process start */
    error = display.Video_Start(DisplayBase::VIDEO_INPUT_CHANNEL_0);
    if (error != DisplayBase::GRAPHICS_OK) {
        printf("Line %d, error %d\n", __LINE__, error);
        while (1);
    }
}

void VideoConverter::stop()
{
    DisplayBase::graphics_error_t error;
    MBED_ASSERT(delegate);
    /* Video write process stop */
    error = display.Video_Stop(DisplayBase::VIDEO_INPUT_CHANNEL_0);
    if (error != DisplayBase::GRAPHICS_OK) {
        printf("Line %d, error %d\n", __LINE__, error);
        while (1);
    }
    
//    thread.terminate();
}

void VideoConverter::waitVsync(const int32_t waitCount)
{
    vsyncCount = waitCount;
    while (vsyncCount) ;
}

void VideoConverter::readAndNotify()
{
    while (1) {
        while ((jcu_encoding == 1) || (image_change == 0)) {
            rtos::Thread::wait(1);
        }
        jcu_buf_index_read = jcu_buf_index_write_done;
        image_change = 0;
        
        delegate->videoConverter_imageAvailable(this, JpegBuffer[jcu_buf_index_read],
                                                jcu_encode_size[jcu_buf_index_read]);
    }
}
