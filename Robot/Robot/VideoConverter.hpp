#ifndef __VideoConverter__
#define __VideoConverter__

#include <cstddef>
#include "DisplayBace.h"
#include "JPEG_Converter.h"
#include "rtos.h"

class VideoConverter {
public:
    class Delegate {
    public:
        virtual void videoConverter_imageAvailable(VideoConverter *sender, uint8_t *jpeg, size_t length) = 0;
    };
    
private:
    VideoConverter();
    static VideoConverter singletonObj;
    
    static constexpr int width  = 640;
    static constexpr int height = 480;
    static constexpr int DATA_SIZE_PER_PIC = 2;
    static constexpr int VIDEO_BUFFER_STRIDE = ((width * DATA_SIZE_PER_PIC) + 31u) & ~31u;
    static constexpr int VIDEO_BUFFER_HEIGHT = height;
    static uint8_t __attribute__((section("NC_BSS"), aligned(16)))
        FrameBuffer_Video[VIDEO_BUFFER_STRIDE * VIDEO_BUFFER_HEIGHT];
    static uint8_t __attribute__((section("NC_BSS"), aligned(8))) JpegBuffer[2][1024 * 50];
    
    static void vsyncHandlerStatic(DisplayBase::int_type_t int_type);
    static void vfieldHandlerStatic(DisplayBase::int_type_t int_type);
    static void jcuHandlerStatic(JPEG_Converter::jpeg_conv_error_t err_code);
    
    volatile int32_t vsyncCount  = 0;
    volatile int32_t vfieldCount = 2;
    JPEG_Converter Jcu;
    size_t jcu_encode_size[2];
    volatile int image_change = 0;
    volatile int jcu_buf_index_write = 0;
    volatile int jcu_buf_index_write_done = 0;
    volatile int jcu_buf_index_read = 0;
    volatile int jcu_encoding = 0;
    JPEG_Converter::bitmap_buff_info_t bitmap_buff_info;
    JPEG_Converter::encode_options_t   encode_options;
    DisplayBase display;
    Delegate *delegate = nullptr;
    unsigned char threadStack[DEFAULT_STACK_SIZE];
    rtos::Thread thread = rtos::Thread(osPriorityNormal, DEFAULT_STACK_SIZE, threadStack);
    
    void waitVsync(const int32_t waitCount);
    void vsyncHandler(DisplayBase::int_type_t int_type);
    void vfieldHandler(DisplayBase::int_type_t int_type);
    void jcuHandler(JPEG_Converter::jpeg_conv_error_t err_code);
    void MBED_NORETURN readAndNotify();
    
public:
    static VideoConverter *singleton();
    void initWithDelegate(Delegate *delegate);
    void start();
    void stop();
};

#endif
