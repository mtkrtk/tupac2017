#include "EthernetTransport.hpp"
#include "RDTPv2.hpp"
#include "AppDelegate.hpp"

int main(int MBED_UNUSED argc, const char MBED_UNUSED * argv[])
{
    mbed::Serial pc(USBTX, USBRX);
    pc.baud(115200);
    EthernetTransport transport;
    RDTPv2 application(&transport);
    AppDelegate delegate(&application);
    
    transport.run();
    
    return 0;
}
