#include "RDTPv2.hpp"
#include "EthernetUtil.hpp"
#import "RDTPPacket.h"
#import <XCTest/XCTest.h>

@interface RobotTest : XCTestCase

@end

@implementation RobotTest

- (void)testEthernetUtil
{
    Endpoint endpoint;
    const char address[] = "192.168.1.5";
    endpoint.set_address(address, 8080);
    XCTAssertEqual(strcmp(address, endpoint.get_address()), 0);
    Transport::Header header = EthernetUtil::endPointToHeader(endpoint);
    XCTAssertEqual(header.address_arr[0], 192);
    XCTAssertEqual(header.address_arr[1], 168);
    XCTAssertEqual(header.address_arr[2], 1);
    XCTAssertEqual(header.address_arr[3], 5);
    XCTAssertEqual(header.port, 8080);
    
    endpoint = EthernetUtil::headerToEndPoint(header);
    XCTAssertEqual(strcmp(address, endpoint.get_address()), 0);
    XCTAssertEqual(endpoint.get_port(), 8080);
}

- (void)testRDTPPacket
{
    RDTPPacket packet;
    int8_t buf[7] = {0};
    int8_t value = 0;
    RDTPPacketComponent component = LeftMotor;
    
    buf[0] = 1 << LeftMotor;
    buf[1] = 0;
    buf[2] = 42;
    RDTPPacket_initWithBytes(&packet, buf, 3);
    XCTAssertEqual(RDTPPacket_getReceiveData(&packet, &value, &component), DataAvailable);
    XCTAssertEqual(value, 42);
    XCTAssertEqual(component, LeftMotor);
    XCTAssertEqual(RDTPPacket_getReceiveData(&packet, &value, &component), EndOfPacket);
    
    buf[0] = 1 << RightMotor | 1 << Servo2;
    buf[1] = 0;
    buf[2] = -42;
    buf[3] = 100;
    RDTPPacket_initWithBytes(&packet, buf, 4);
    XCTAssertEqual(RDTPPacket_getReceiveData(&packet, &value, &component), DataAvailable);
    XCTAssertEqual(value, -42);
    XCTAssertEqual(component, RightMotor);
    XCTAssertEqual(RDTPPacket_getReceiveData(&packet, &value, &component), DataAvailable);
    XCTAssertEqual(value, 100);
    XCTAssertEqual(component, Servo2);
    XCTAssertEqual(RDTPPacket_getReceiveData(&packet, &value, &component), EndOfPacket);
    
    buf[0] = 0;
    buf[1] = 0;
    buf[2] = StartVideo;
    RDTPPacket_initWithBytes(&packet, buf, 3);
    XCTAssertEqual(RDTPPacket_getReceiveData(&packet, &value, &component), CommandAvailable);
    XCTAssertEqual(RDTPPacket_getReceiveCommand(&packet), StartVideo);
    
    buf[0] = 1 << Servo0;
    buf[1] = 0;
    buf[2] = 100;
    RDTPPacket_initWithBytes(&packet, buf, 3);
    XCTAssertEqual(RDTPPacket_getReceiveData(&packet, &value, &component), DataAvailable);
    XCTAssertEqual(value, 100);
    XCTAssertEqual(component, Servo0);
    XCTAssertEqual(RDTPPacket_getReceiveData(&packet, &value, &component), EndOfPacket);
}

@end
